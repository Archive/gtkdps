/* gtkDPScontext.h
 * Copyright (C) 1997, 1998 GYVE Development Team
 *
 * Author: Terumoto 'tel' HAYAKAWA <hayakawa@cv.cs.ritsumei.ac.jp>
 * Created: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GTK__DPS_CONTEXT_H
#define GTK__DPS_CONTEXT_H 


#include <gdk/gdk.h>
#include <gtk/gtkdata.h>

#include <gtkDPS/gdkDPS.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_DPS_CONTEXT(obj)          GTK_CHECK_CAST (obj, gtk_dps_context_get_type (), GtkDPSContext)
#define GTK_DPS_CONTEXT_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_dps_context_get_type (), GtkDPSContextClass)
#define GTK_IS_DPS_CONTEXT(obj)       GTK_CHECK_TYPE (obj, gtk_dps_context_get_type ())


typedef struct _GtkDPSContext       GtkDPSContext;
typedef struct _GtkDPSContextClass  GtkDPSContextClass;

struct _GtkDPSContext
{
  GtkData data;
  GdkDPSContext *gdk_dps_context;
};

struct _GtkDPSContextClass
{
  GtkDataClass parent_class;

  void (* enter_current_notify) (GtkDPSContext *gtk_dps_context,
				 GdkDrawable *gdk_drawable);
  void (* leave_current_notify) (GtkDPSContext *gtk_dps_context,
				 GdkDrawable *gdk_drawable);
};


guint      gtk_dps_context_get_type      (void);
GtkObject* gtk_dps_context_new           (GdkWindow *);
GdkVisual* gtk_dps_context_get_visual    (GtkDPSContext *);
gint       gtk_dps_context_enter_current (GtkDPSContext *, GdkDrawable *);
gint       gtk_dps_context_leave_current (GtkDPSContext *, GdkDrawable *);

void       gtk_dps_set_context           (GtkDPSContext *);

GtkDPSContext* gtk_dps_get_current_context     (void);
GdkDrawable*   gtk_dps_get_current_drawable    (void);   
DPSContext     gtk_dps_get_current_raw_context (void);

void       gtk_dps_flush         (GtkDPSContext *);
#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* GTK__DPS_CONTEXT_H */
