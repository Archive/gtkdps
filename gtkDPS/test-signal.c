/*
 * gtkDPS single context test program by Terumoto HAYAKAWA
 * 
 */ 
#include <gtk/gtk.h>
#include "gtkDPS.h"
#include <DPS/psops.h>
#include <DPS/dpsXclient.h>
#include <DPS/dpsops.h>

#ifdef HAVE_DPS_NXAGENT    
#include <DPS/dpsNXargs.h>
#endif

/* 
 * This is callback function. 
 * The data arguments are ignored in this example..
 * More on callbacks below. 
 */
static void
dialog_close_callback (GtkWidget *widget,
		       gpointer   data)
{
    gtk_main_quit();
}

/*
 * This callback function is to draw a square. 
 */
static void
draw_square (GtkWidget *widget,
	     gpointer   data)
{
  DPSContext ctxt;
  float i;

  /*
   * This is called in all case of using dps area.
   * DPS function is called between gtk_dps_area_begin() and
   * gtk_dps_area_end() function.
   */
  gtk_dps_area_begin(GTK_DPS_AREA (widget));
  {
    ctxt = gtk_dps_get_current_raw_context();
    for (i = 1.0; i > 0.0; i = i - 0.01)
      {
	DPSnewpath(ctxt);
	DPSsetrgbcolor(ctxt, i, 0.0, 0.0);
	DPSmoveto(ctxt, 100.0, 100.0);
	DPSlineto(ctxt, i*100.0 + 100.0, 100.0);
	DPSlineto(ctxt, i*100.0 + 100.0, i*100.0 + 100.0);
	DPSlineto(ctxt, 100.0, i*100.0 + 100.0);
	DPSclosepath(ctxt);
	DPSfill(ctxt);
      }
#if 1
    /* draw small triangle at lower left corner */
    DPSsetrgbcolor(ctxt, 0.0, 0.0, 0.0);
    DPSnewpath(ctxt);
    DPSmoveto(ctxt, 0.0, 0.0);
    DPSlineto(ctxt, 100.0, 50.0);
    DPSlineto(ctxt, 50.0, 100.0);
    DPSclosepath(ctxt);
    DPSfill(ctxt);
#endif
  }
  /* flush the dps area */
  gtk_dps_area_flush(GTK_DPS_AREA (widget));
  /*
   * This is called in all case of using dps area.
   */
  gtk_dps_area_end(GTK_DPS_AREA (widget));

}

/*
 * This callback function is to draw a circle. 
 */
static void
draw_circle (GtkWidget *widget,
	     gpointer   data)
{
  DPSContext ctxt;
  float i;

  gtk_dps_area_begin(GTK_DPS_AREA (widget));
  {
    ctxt = gtk_dps_get_current_raw_context();
    for (i = 1.0; i > 0.0; i = i - 0.01)
      {
	DPSnewpath(ctxt);
	DPSarc(ctxt, 200.0, 200.0, i * 50.0, 0.0, 360.0);
	DPSsetrgbcolor(ctxt, 1.0, i, 1.0);
	DPSfill(ctxt);
      }
  }
  gtk_dps_area_flush(GTK_DPS_AREA (widget));
  gtk_dps_area_end(GTK_DPS_AREA (widget));
}


/*
 * This callback function is to draw a line.
 */
static void
draw_line (GtkWidget *widget,
	   gpointer   data)
{
  DPSContext ctxt;

  gtk_dps_area_begin(GTK_DPS_AREA (widget));
  {
    ctxt = gtk_dps_get_current_raw_context();
    DPSnewpath(ctxt);
    DPSmoveto(ctxt, 0.0, 0.0);
    DPSsetrgbcolor(ctxt, 0.0, 0.0, 1.0);
    DPSlineto(ctxt, 300.0, 300.0);
    DPSstroke(ctxt);
  }
  gtk_dps_area_flush(GTK_DPS_AREA (widget));
  gtk_dps_area_end(GTK_DPS_AREA (widget));

}

/*
 * main function
 */
int
main(int argc, char **argv)
{
  /* GtkWidget is the storage type for widgets */
    GtkWidget *dialog;
    GtkWidget *close_button;
    GtkWidget *DPS_area;
    GtkWidget *square_button;
    GtkWidget *line_button;
    GtkWidget *circle_button;
    GtkWidget *window;
    

    /*
     * This is called in all GTK applications. Arguments are parsed from
     * the command line and are returned to the application.
     */
    gtk_init(&argc, &argv);

#if HAVE_DPS_NXAGENT    
  fprintf(stderr, "Set DPS NXAGENT to lauch automatically...");
  XDPSNXSetClientArg(XDPSNX_AUTO_LAUNCH, (void *)True);
  fprintf(stderr, "done\n");
#endif /* HAVE_DPS_NXAGENT */

    /* create a new window */
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    /* create a new dps area */
    DPS_area = gtk_dps_area_new();

    /* This packs the button into the window (a gtk container). */
    gtk_container_add(GTK_CONTAINER(window), DPS_area);

    /* set the DPS_area size */
    gtk_dps_area_size(GTK_DPS_AREA(DPS_area), 500, 500);

    /* bind an action to DPS_area */
    gtk_widget_set_events(DPS_area, GDK_EXPOSURE_MASK);

    /* display DPS_area */
    gtk_widget_show(DPS_area);

    /* display this window */
    gtk_widget_show(window);



    /* create a new dialog */
    dialog = gtk_dialog_new();


    /* create a new button with the label "Close". */
    close_button = gtk_button_new_with_label("Close");
    /*
     * Instead of gtk_container_add, we pack this close_button into the 
     * invisible box, which has been packed into the dialog.
     */
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
                       close_button, TRUE, TRUE, 0);
    /*
     * When the button receives the "clicked" signal, it will call the
     * function dialog_close_callback() passing it NULL as it's argument.
     * The dialog_close_callback() function is defined above.
     */
    gtk_signal_connect(GTK_OBJECT(close_button), "clicked",
                       (GtkSignalFunc)dialog_close_callback,
                       NULL);
    /* display close_button */
    gtk_widget_show(close_button);


    square_button = gtk_button_new_with_label("square");
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
                       square_button, TRUE, TRUE, 0);
    gtk_signal_connect_object(GTK_OBJECT(square_button), "clicked",
			      (GtkSignalFunc)draw_square,
			      GTK_OBJECT(DPS_area));
    gtk_widget_show(square_button);


    line_button = gtk_button_new_with_label("line");
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
                       line_button, TRUE, TRUE, 0);
    gtk_signal_connect_object(GTK_OBJECT(line_button), "clicked",
			      (GtkSignalFunc)draw_line,
			      GTK_OBJECT(DPS_area));
    gtk_widget_show(line_button);

    circle_button = gtk_button_new_with_label("circle");
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
                       circle_button, TRUE, TRUE, 0);
    gtk_signal_connect_object(GTK_OBJECT(circle_button), "clicked",
			      (GtkSignalFunc)draw_circle,
			      GTK_OBJECT(DPS_area));
    gtk_widget_show(circle_button);

    /* display the dialog */
    gtk_widget_show(dialog);

    /*
     * All GTK applications must have a gtk_main(). Control ends here
     * and waits for an event to occur (like a key press or mouse event).
     */
    gtk_main();

    return 0;
}
