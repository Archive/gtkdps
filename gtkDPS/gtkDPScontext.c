/* gtkDPScontext.c
 * Copyright (C) 1997, 1998 GYVE Development Team
 *
 * Author: Terumoto 'tel' HAYAKAWA <hayakawa@cv.cs.ritsumei.ac.jp>
 * Created: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <gdk/gdk.h>
#include "gdkDPS.h"

#include "gtkDPScontext.h"
#include <gtk/gtksignal.h>

/* for gtk_widget_get_default_visual() */
#include <gtk/gtkwidget.h>


enum {
  ENTER_CURRENT_NOTIFY,
  LEAVE_CURRENT_NOTIFY,
  LAST_SIGNAL
};


typedef void (*GtkDPSContextSignal) (GtkObject *object,
				     gpointer   arg1,
				     gpointer   data);


static void gtk_dps_context_marshal_signal (GtkObject      *object,
					    GtkSignalFunc   func,
					    gpointer        func_data,
					    GtkArg         *args);


static void gtk_dps_context_class_init (GtkDPSContextClass *klass);
static void gtk_dps_context_init       (GtkDPSContext      *gtk_dps_context);
static void gtk_dps_context_enter_current_notify
		(GtkDPSContext *gtk_dps_context, GdkDrawable *gdk_drawable);
static void gtk_dps_context_leave_current_notify
		(GtkDPSContext *gtk_dps_context, GdkDrawable *gdk_drawable);
static void gtk_real_dps_context_enter_current_notify
		(GtkDPSContext *gtk_dps_context, GdkDrawable *gdk_drawable);
static void gtk_real_dps_context_leave_current_notify
		(GtkDPSContext *gtk_dps_context, GdkDrawable *gdk_drawable);

static gint gtk_dps_check_gtk_dps_context_stack (void);
#if 0
static gint gtk_dps_check_gdk_drawable_stack   (void);
#endif
static void gtk_dps_push_gtk_dps_context (GtkDPSContext *gtk_dps_context);
static void gtk_dps_push_gdk_drawable   (GdkDrawable *gdk_drawable);
static void gtk_dps_pop_gtk_dps_context  (GtkDPSContext **gtk_dps_context);
static void gtk_dps_pop_gdk_drawable    (GdkDrawable **gdk_drawable);


static gint dps_context_signals[LAST_SIGNAL] = { 0 };

static GtkDPSContext *gtk_dps_current_gtk_dps_context = NULL;
static GdkDrawable  *gtk_dps_current_gdk_drawable = NULL;

static GSList *gtk_dps_gtk_dps_context_stack = NULL;
static GSList *gtk_dps_gdk_drawable_stack = NULL;

guint
gtk_dps_context_get_type ()
{
  static guint gtk_dps_context_type = 0;

  if (!gtk_dps_context_type)
    {
      GtkTypeInfo gtk_dps_context_info =
      {
	"GtkDPSContext",
	sizeof (GtkDPSContext),
	sizeof (GtkDPSContextClass),
	(GtkClassInitFunc) gtk_dps_context_class_init,
	(GtkObjectInitFunc) gtk_dps_context_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      gtk_dps_context_type = gtk_type_unique (gtk_data_get_type (),
					      &gtk_dps_context_info);
    }

  return gtk_dps_context_type;
}

static void
gtk_dps_context_class_init (GtkDPSContextClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  dps_context_signals[ENTER_CURRENT_NOTIFY] =
      gtk_signal_new ("enter_current_notify",
		      GTK_RUN_FIRST,
		      object_class->type,
		      GTK_SIGNAL_OFFSET (GtkDPSContextClass,
					 enter_current_notify),
		      gtk_dps_context_marshal_signal,
		      GTK_TYPE_NONE, 0);

  dps_context_signals[LEAVE_CURRENT_NOTIFY] =
      gtk_signal_new ("leave_current_notify",
		      GTK_RUN_FIRST,
		      object_class->type,
		      GTK_SIGNAL_OFFSET (GtkDPSContextClass,
					 leave_current_notify),
		      gtk_dps_context_marshal_signal,
		      GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, dps_context_signals, LAST_SIGNAL);

  class->enter_current_notify = gtk_real_dps_context_enter_current_notify;
  class->leave_current_notify = gtk_real_dps_context_leave_current_notify;
}

static void
gtk_dps_context_init (GtkDPSContext *gtk_dps_context)
{
  gtk_dps_context->gdk_dps_context = NULL;
}

GtkObject*
gtk_dps_context_new (GdkWindow *gdk_window)
{
  GtkDPSContext *gtk_dps_context;

  g_return_val_if_fail(gdk_window != NULL, NULL);

  gtk_dps_context = gtk_type_new (gtk_dps_context_get_type ());

  gtk_dps_context->gdk_dps_context
      = gdk_dps_context_new(gdk_window);

  return GTK_OBJECT (gtk_dps_context);
}


static void
gtk_dps_context_enter_current_notify (GtkDPSContext *gtk_dps_context,
				      GdkDrawable *gdk_drawable)
{
  gtk_signal_emit(GTK_OBJECT (gtk_dps_context),
		  dps_context_signals[ENTER_CURRENT_NOTIFY],
		  (gpointer)gdk_drawable);
}

static void
gtk_dps_context_leave_current_notify (GtkDPSContext *gtk_dps_context,
				      GdkDrawable *gdk_drawable)
{
  gtk_signal_emit(GTK_OBJECT (gtk_dps_context),
		  dps_context_signals[LEAVE_CURRENT_NOTIFY],
		  (gpointer)gdk_drawable);
}

static void
gtk_dps_context_marshal_signal (GtkObject      *object,
				GtkSignalFunc   func,
				gpointer        func_data,
				GtkArg         *args)
{
  GtkDPSContextSignal rfunc;

  rfunc = (GtkDPSContextSignal) func;

  (* rfunc) (object, GTK_VALUE_POINTER (args[0]), func_data);
}

static void
gtk_real_dps_context_enter_current_notify (GtkDPSContext *gtk_dps_context,
					   GdkDrawable *gdk_drawable)
{
}

static void
gtk_real_dps_context_leave_current_notify (GtkDPSContext *gtk_dps_context,
					   GdkDrawable *gdk_drawable)
{
}


gint
gtk_dps_context_enter_current (GtkDPSContext *gtk_dps_context,
			       GdkDrawable *gdk_drawable)
{
  gint return_val;
  gint r;
  GdkDPSContext *gdk_dps_context;

  g_return_val_if_fail(gtk_dps_context != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_DPS_CONTEXT (gtk_dps_context), FALSE);
  g_return_val_if_fail(gdk_drawable != NULL, FALSE);
  
  /* The "1 ||" below is a temporary solution.  After calling
   * gtk_dps_set_context(), gtk_dps_current_gtk_dps_context has
   * non-NULL value, so it complains...
   * very kludgely.		-eiichi
   */
  if (1 || ((gtk_dps_current_gtk_dps_context == NULL)
	  && (gtk_dps_current_gdk_drawable == NULL))) {

    gdk_dps_context = gtk_dps_context->gdk_dps_context;
      
    r = gdk_dps_set_current(gdk_dps_context, gdk_drawable);
    if (r == TRUE) {
      gtk_dps_current_gtk_dps_context = gtk_dps_context;
      gtk_dps_current_gdk_drawable = gdk_drawable;
      gtk_dps_context_enter_current_notify(gtk_dps_context, gdk_drawable);
    }
    return_val = r;
  } else {
    if (gtk_dps_current_gtk_dps_context != NULL) {
      g_print("gtk_dps_current_gtk_dps_context != NULL\n");
    }
    if (gtk_dps_current_gdk_drawable != NULL) {
      g_print("gtk_dps_current_gdk_drawable != NULL\n");
    }
    return_val = FALSE;
  }

  return return_val;

}

gint
gtk_dps_context_leave_current (GtkDPSContext *gtk_dps_context,
			       GdkDrawable *gdk_drawable)
{
  gint return_val;
  gint r;

  g_return_val_if_fail(gtk_dps_context != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_DPS_CONTEXT (gtk_dps_context), FALSE);
  g_return_val_if_fail(gdk_drawable != NULL, FALSE);

  if ((gtk_dps_current_gtk_dps_context == gtk_dps_context)
      && (gtk_dps_current_gdk_drawable == gdk_drawable))
  {
      r = TRUE;
      if (r == TRUE) {
	  gtk_dps_current_gtk_dps_context = NULL;
	  gtk_dps_current_gdk_drawable = NULL;
	  gtk_dps_context_leave_current_notify(gtk_dps_context, gdk_drawable);
      }
      return_val = r;
  } else {
      if (gtk_dps_current_gtk_dps_context != gtk_dps_context) {
	  g_print("gtk_dps_current_gtk_dps_context != gtk_dps_context\n");
      }
      if (gtk_dps_current_gdk_drawable != gdk_drawable) {
	  g_print("gtk_dps_current_gdk_drawable != gdk_drawable\n");
      }
      return_val = FALSE;
  }

  return return_val;
}

void
gtk_dps_set_context (GtkDPSContext *gtk_dps_context)
{
  gtk_dps_current_gtk_dps_context = gtk_dps_context;

  gdk_dps_set_context(gtk_dps_context->gdk_dps_context);
}

GtkDPSContext *
gtk_dps_get_current_context ()
{
  return gtk_dps_current_gtk_dps_context;
}

GdkDrawable *
gtk_dps_get_current_drawable ()
{
  return gtk_dps_current_gdk_drawable;
}

DPSContext
gtk_dps_get_current_raw_context ()
{
  return *gdk_dps_get_current_context()->dps_context;
}


void
gtk_dps_flush(GtkDPSContext *gtk_dps_context)
{
  g_return_if_fail(gtk_dps_context != NULL);
  g_return_if_fail(GTK_IS_DPS_CONTEXT (gtk_dps_context));

  gdk_dps_flush_context(gtk_dps_context->gdk_dps_context);
}



static gint
gtk_dps_check_gtk_dps_context_stack ()
{
  if (gtk_dps_gtk_dps_context_stack != NULL) {
      return TRUE;
  } else {
      return FALSE;
  }
}

static void
gtk_dps_push_gtk_dps_context (GtkDPSContext *gtk_dps_context)
{
  gtk_dps_gtk_dps_context_stack
      = g_slist_prepend(gtk_dps_gtk_dps_context_stack, gtk_dps_context);
}

static void
gtk_dps_pop_gtk_dps_context (GtkDPSContext **ret_gtk_dps_context)
{
  GSList *tmp;
  GtkDPSContext *gtk_dps_context;

  if (gtk_dps_gtk_dps_context_stack) {
      tmp = gtk_dps_gtk_dps_context_stack;
      gtk_dps_context = tmp->data;
      gtk_dps_gtk_dps_context_stack = tmp->next;
      g_slist_free_1(tmp);
      *ret_gtk_dps_context = gtk_dps_context;
  } else {
      *ret_gtk_dps_context = NULL;
  }
}

#if 0
static gint
gtk_dps_check_gdk_drawable_stack ()
{
  if (gtk_dps_gdk_drawable_stack != NULL) {
      return TRUE;
  } else {
      return FALSE;
  }
}
#endif

static void
gtk_dps_push_gdk_drawable (GdkDrawable *gdk_drawable)
{
  gtk_dps_gdk_drawable_stack
      = g_slist_prepend(gtk_dps_gdk_drawable_stack, gdk_drawable);
}

static void
gtk_dps_pop_gdk_drawable (GdkDrawable **ret_gdk_drawable)
{
  GSList *tmp;
  GdkDrawable *gdk_drawable;

  if (gtk_dps_gdk_drawable_stack) {
      tmp = gtk_dps_gdk_drawable_stack;
      gdk_drawable = tmp->data;
      gtk_dps_gdk_drawable_stack = tmp->next;
      g_slist_free_1(tmp);
      *ret_gdk_drawable = gdk_drawable;
  } else {
      *ret_gdk_drawable = NULL;
  }
}

