/* fonts.h generated from fonts.psw
   by unix pswrap V1.009  Wed Apr 19 17:50:24 PDT 1989
 */

#ifndef FONTS_H
#define FONTS_H

#if  defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

extern void PSWFontNames(int *noOfFonts);

extern void PSWGetFontsArray(char *fontname, char *fileOrFont);

extern void PSWCompleteFilename(const char *filename, int *found, char *completePath);

extern void PSWProduct(char *productname);

extern void PSWGetFontAndType(char *fontname, int *fonttype);

extern void PSWSetFont(const char *font, const float fontMatrix[]);

extern void PSWGetFontType(const char *font, int *fontType);

extern void PSWShowSampleString(DPSContext context, const char *font, int size, const char *sample);

#if  defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif /* FONTS_H */
