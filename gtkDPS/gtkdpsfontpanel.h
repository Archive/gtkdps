/* gdkdpsfontpanel.h --- DPS font panel widget
 * Copyright (C) 1998 Hideki FUJIMOTO
 *
 * Author: Hideki FUJIMOTO <hideki70@osk2.3web.ne.jp>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_DPS_FONTPANEL(obj)  GTK_CHECK_CAST (obj, gtk_dps_fontpanel_get_type (), GtkDpsFontpanel)
#define GTK_DPS_FONTPANEL_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_dps_fontpanel_get_type (), GtkDpsFontpanelClass)
#define GTK_IS_DPS_FONTPANEL(obj)       GTK_CHECK_TYPE (obj, gtk_dps_fontpanel_get_type ())

typedef struct _GtkDpsFontpanel       GtkDpsFontpanel;
typedef struct _GtkDpsFontpanelClass  GtkDpsFontpanelClass;

struct _GtkDpsFontpanel
{
	GtkVBox    vbox;
	GtkWidget  *main_box;
	GtkWidget  *dps_area;
	GtkWidget  *family_box;
	GtkWidget  *face_box;
	GtkWidget  *size_entry;
	GtkWidget  *input_entry;
	GtkWidget  *ok_button;
	GtkWidget  *close_button;
	gchar      *font_name;
	gint       font_size;
	gchar      *input_string;
};

struct _GtkDpsFontpanelClass
{
	GtkVBoxClass parent_class;
};

guint gtk_dps_fontpanel_get_type ();
GtkWidget *gtk_dps_fontpanel_new (void);
gchar *gtk_dps_fontpanel_get_fontname (GtkDpsFontpanel *fontpanel);
gint gtk_dps_fontpanel_get_fontsize (GtkDpsFontpanel *fontpanel);
gchar *gtk_dps_fontpanel_get_input (GtkDpsFontpanel *fontpanel);

#ifdef __cplusplus
}
#endif /* __cplusplus */





