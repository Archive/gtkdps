/* gtkDPSarea.c
 * Copyright (C) 1997, 1998 GYVE Development Team
 *
 * Author: Terumoto 'tel' HAYAKAWA <hayakawa@cv.cs.ritsumei.ac.jp>
 * Created: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gdkDPS_gdkutils.h"

#include "gtkDPScontext.h"
#include "gtkDPSwidget.h"
#include "gtkDPSarea.h"

static void gtk_dps_area_class_init    (GtkDPSAreaClass *klass);
static void gtk_dps_area_init          (GtkDPSArea      *DPS_area);
static void gtk_dps_area_realize       (GtkWidget       *widget);
static void gtk_dps_area_size_allocate (GtkWidget       *widget,
					GtkAllocation   *allocation);

const char *
gtkDPS_version()
{
  return VERSION;
}

guint
gtk_dps_area_get_type ()
{
  static guint dps_area_type = 0;

  if (!dps_area_type)
    {
      GtkTypeInfo dps_area_info =
      {
	"GtkDPSArea",
	sizeof (GtkDPSArea),
	sizeof (GtkDPSAreaClass),
	(GtkClassInitFunc) gtk_dps_area_class_init,
	(GtkObjectInitFunc) gtk_dps_area_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL 
      };

      dps_area_type = gtk_type_unique (gtk_dps_widget_get_type (), &dps_area_info);
    }

  return dps_area_type;
}

static void
gtk_dps_area_class_init (GtkDPSAreaClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkDPSWidgetClass *dps_widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;
  dps_widget_class = (GtkDPSWidgetClass*) class;

  widget_class->realize = gtk_dps_area_realize;
  widget_class->size_allocate = gtk_dps_area_size_allocate;
}

static void
gtk_dps_area_init (GtkDPSArea *dps_area)
{
  GTK_WIDGET_SET_FLAGS (dps_area, GTK_BASIC);
}


GtkWidget*
gtk_dps_area_new ()
{
  return GTK_WIDGET (gtk_type_new (gtk_dps_area_get_type ()));
}

void
gtk_dps_area_size (GtkDPSArea *dps_area, 
		   gint width, 
		   gint height)
{
  g_return_if_fail (dps_area != NULL);
  g_return_if_fail (GTK_IS_DPS_AREA (dps_area));

  GTK_WIDGET (dps_area)->requisition.width = width;
  GTK_WIDGET (dps_area)->requisition.height = height;
}

static void
gtk_dps_area_realize (GtkWidget *widget)
{
  GtkDPSArea *dps_area;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_DPS_AREA (widget));

  dps_area = GTK_DPS_AREA (widget);
  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget);

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

#if 0
  widget->window = gdk_window_new (widget->parent->window,
				   &attributes, attributes_mask);
#else  /* For GTK-0.99.3 
	  (suggested by AOSASA Shigeru <aozasa@sakuranet.or.jp>) */
  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
				   &attributes, attributes_mask);
#endif 

  gdk_window_set_user_data (widget->window, dps_area);

  widget->style = gtk_style_attach (widget->style, widget->window);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);
}


static void
gtk_dps_area_size_allocate (GtkWidget     *widget,
			    GtkAllocation *allocation)
{
  GdkEventConfigure event;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_DPS_AREA (widget));
  g_return_if_fail (allocation != NULL);

  widget->allocation = *allocation;

  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
			      allocation->x, allocation->y,
			      allocation->width, allocation->height);

      event.type = GDK_CONFIGURE;
      event.window = widget->window;
      event.x = allocation->x;
      event.y = allocation->y;
      event.width = allocation->width;
      event.height = allocation->height;

      gtk_widget_event (widget, (GdkEvent*) &event);
    }
}


void
gtk_dps_area_begin (GtkDPSArea *dps_area)
{
  GdkDPSContext *gdk_dps_context;

  g_return_if_fail (dps_area != NULL);
  g_return_if_fail (GTK_IS_DPS_AREA (dps_area));
  g_return_if_fail (GTK_WIDGET_REALIZED (dps_area));
  
  if(GTK_DPS_WIDGET (dps_area)->gtk_dps_context == NULL) {
    GTK_DPS_WIDGET (dps_area)->gtk_dps_context
      =  GTK_DPS_CONTEXT (gtk_dps_context_new(GTK_WIDGET(dps_area)->window));
  }
  gtk_dps_set_context (GTK_DPS_WIDGET (dps_area)->gtk_dps_context);
  gtk_dps_context_enter_current (GTK_DPS_WIDGET (dps_area)->gtk_dps_context,
				 GTK_WIDGET (dps_area)->window);
}

void
gtk_dps_area_end (GtkDPSArea *dps_area)
{
  g_return_if_fail (dps_area != NULL);
  g_return_if_fail (GTK_IS_DPS_AREA (dps_area));
  g_return_if_fail (GTK_WIDGET_REALIZED (dps_area));

  gtk_dps_context_leave_current (GTK_DPS_WIDGET (dps_area)->gtk_dps_context,
				 GTK_WIDGET (dps_area)->window);
}

void
gtk_dps_area_flush (GtkDPSArea *dps_area)
{
  g_return_if_fail (dps_area != NULL);
  g_return_if_fail (GTK_IS_DPS_AREA (dps_area));
  g_return_if_fail (GTK_DPS_WIDGET (dps_area)->gtk_dps_context != NULL);

  gtk_dps_flush(GTK_DPS_WIDGET (dps_area)->gtk_dps_context);

}
