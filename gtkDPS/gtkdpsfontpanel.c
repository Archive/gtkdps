/* gdkdpsfontpanel.c --- DPS font panel widget
 * Copyright (C) 1998 Hideki FUJIMOTO
 *
 * Author: Hideki FUJIMOTO <hideki70@osk2.3web.ne.jp>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>

#include <gtkDPS/gtkDPS.h>
#include <DPS/dpsops.h>
#include <DPS/PSres.h>
#include <DPS/psops.h>
#include <DPS/dpsfriends.h>
#include <DPS/dpsXclient.h>

#ifdef HAVE_DPS_NXAGENT
#include <DPS/dpsNXargs.h>
#endif

#include "fonts.h"
#include "gtkdpsfontpanel.h"

#ifdef _NO_PROTO
#define ARGCAST int
#else
#define ARGCAST void *
#endif

#define DEBUG 1

#define MAX_SIZE 1024
#define AREA_SIZE_HEIGHT  128
#define AREA_SIZE_WIDTH   288
#define LIST_WIDTH        120
#define LIST_HEIGHT       140
#define SIZE_ENTRY_HEIGHT 60
#define SIZE_ENTRY_WIDTH  20


typedef struct SList
  {
    struct SList *next;
    char *data;
  }
SList;

typedef struct TypefaceList
  {
    struct TypefaceList *next;
    char *typeface;
    char *fontname;
  }
TypefaceList;

typedef struct GPSFontList
  {
    struct GPSFontList *next;
    char *family;
    struct TypefaceList *typeface;
  }
GPSFontList;

static void gtk_dps_fontpanel_class_init (GtkDpsFontpanelClass * class);
static void gtk_dps_fontpanel_init (GtkDpsFontpanel * fontpanel);
static void gtk_dps_fontpanel_destroy (GtkObject * object);
static void gtk_dps_fontpanel_realize (GtkWidget * widget);
static void init_dpsarea (GtkDpsFontpanel * fontpanel, GtkWidget * parent);
static void init_font_family (GtkDpsFontpanel * fontpanel, GtkWidget * parent);
static void init_font_face (GtkDpsFontpanel * fontpanel, GtkWidget * parent);
static void init_font_size (GtkDpsFontpanel * fontpanel, GtkWidget * parent);
static void load_fonts (GtkWidget * widget, GtkWidget * box);
static void load_dgs_fonts (GtkWidget * widget);
static void load_dps_fonts (GtkWidget * widget);
static void init_dgs_fonts (DPSContext ctxt);
static int init_dps_fonts (char *resourceType, char *resourceName, char *resourceFile,
			   char *output_list);
static GPSFontList *init_gtkdps_font_list (void);
static void load_font_list (GtkWidget * widget);
static void change_family (GtkWidget * widget, GdkEventButton * event, gpointer data);
static void change_typeface (GtkWidget * widget, GdkEventButton * event, gpointer data);
static void change_size (GtkWidget * widget, GdkEventButton * event, gpointer data);
static void preview_font (GtkWidget * button_widget, void *user_data);
static void sample_string_drawer_core (GtkWidget * widget, char *name, int size,
				       char *string);
static int is_aladdin_product (char *product_name);
static char *get_typeface (char *pathname);
static int sort_by_name (const char **file1, const char **file2);
static void expose_handler (GtkWidget * widget, GdkEventButton * event);


static GtkVBoxClass *dps_fontpanel_parent_class;
static DPSContext context;
static GPSFontList *dps_fonts;
static SList *font_name_list;

guint
gtk_dps_fontpanel_get_type ()
{
  static guint dps_fontpanel_type = 0;

  if (!dps_fontpanel_type)
    {
      GtkTypeInfo dpsfontpanel_info =
      {
	"GtkDpsFontpanel",
	sizeof (GtkDpsFontpanel),
	sizeof (GtkDpsFontpanelClass),
	(GtkClassInitFunc) gtk_dps_fontpanel_class_init,
	(GtkObjectInitFunc) gtk_dps_fontpanel_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL,
      };

      dps_fontpanel_type = gtk_type_unique (gtk_vbox_get_type (), &dpsfontpanel_info);
    }

  return dps_fontpanel_type;
}

static void
gtk_dps_fontpanel_class_init (GtkDpsFontpanelClass * kclass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;


  object_class = (GtkObjectClass *) kclass;
  widget_class = (GtkWidgetClass *) kclass;

  dps_fontpanel_parent_class = gtk_type_class (gtk_vbox_get_type ());
  widget_class->realize = gtk_dps_fontpanel_realize;
  object_class->destroy = gtk_dps_fontpanel_destroy;
}

static void
gtk_dps_fontpanel_init (GtkDpsFontpanel * fontpanel)
{
  GtkWidget *frame;
  GtkWidget *display_box;
  GtkWidget *selection_box;
  GtkWidget *hbox;
  GtkWidget *separator;
  GtkWidget *button;

  fontpanel->font_name = NULL;
  fontpanel->font_size = 0;
  fontpanel->input_string = NULL;

  fontpanel->main_box = gtk_vbox_new (FALSE, 6);
  gtk_container_add (GTK_CONTAINER (fontpanel), fontpanel->main_box);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
  gtk_box_pack_start (GTK_BOX (fontpanel->main_box), frame, FALSE, FALSE, 0);
  gtk_widget_show (frame);

  display_box = gtk_vbox_new (FALSE, 2);
  gtk_container_border_width (GTK_CONTAINER (display_box), 8);
  gtk_container_add (GTK_CONTAINER (frame), display_box);
  gtk_widget_show (display_box);

  init_dpsarea (fontpanel, display_box);

  selection_box = gtk_vbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (fontpanel->main_box), selection_box, FALSE, FALSE, 0);
  gtk_widget_show (selection_box);

  hbox = gtk_hbox_new (FALSE, 3);
  gtk_widget_set_usize (hbox, 300, 160);
  gtk_box_pack_start (GTK_BOX (selection_box), hbox, FALSE, FALSE, 0);
  gtk_widget_show (hbox);

  init_font_family (fontpanel, hbox);
  init_font_face (fontpanel, hbox);
  init_font_size (fontpanel, hbox);

  separator = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (fontpanel->main_box), separator, FALSE, TRUE, 0);
  gtk_widget_show (separator);

  fontpanel->input_entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (fontpanel->main_box), fontpanel->input_entry,
		      FALSE, FALSE, 0);
  gtk_widget_show (fontpanel->input_entry);

  hbox = gtk_hbox_new (FALSE, 6);
  gtk_box_pack_start (GTK_BOX (fontpanel->main_box), hbox, TRUE, TRUE, 0);
  gtk_widget_show (hbox);

  fontpanel->ok_button = gtk_button_new_with_label ("  Ok  ");
  gtk_box_pack_end (GTK_BOX (hbox), fontpanel->ok_button, FALSE, FALSE, 0);
  gtk_widget_show (fontpanel->ok_button);

  fontpanel->close_button = gtk_button_new_with_label ("  Close  ");
  gtk_box_pack_end (GTK_BOX (hbox), fontpanel->close_button, FALSE, FALSE, 0);
  gtk_widget_show (fontpanel->close_button);

  button = gtk_button_new_with_label ("  Preview  ");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) preview_font, fontpanel);
  gtk_box_pack_end (GTK_BOX (hbox), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  /* Show all */
  gtk_widget_show (fontpanel->main_box);
}

GtkWidget *
gtk_dps_fontpanel_new (void)
{
  GtkDpsFontpanel *fontpanel;

  fontpanel = gtk_type_new (gtk_dps_fontpanel_get_type ());

  return GTK_WIDGET (fontpanel);
}


gchar *
gtk_dps_fontpanel_get_fontname (GtkDpsFontpanel * fontpanel)
{
  return fontpanel->font_name;
}

gint
gtk_dps_fontpanel_get_fontsize (GtkDpsFontpanel * fontpanel)
{
  char *str;

  str = gtk_entry_get_text (GTK_ENTRY (fontpanel->size_entry));
  if (strlen (str) < 1)
    {
      return 0;
    }
  else
    {
      fontpanel->font_size = atoi (str);
    }

  return fontpanel->font_size;
}

gchar *
gtk_dps_fontpanel_get_input (GtkDpsFontpanel * fontpanel)
{
  char *str;

  str = gtk_entry_get_text (GTK_ENTRY (fontpanel->input_entry));
  if (strlen (str) < 1)
    {
      return NULL;
    }
  else
    {
      if (fontpanel->input_string)
	g_free (fontpanel->input_string);
      fontpanel->input_string = (char *) g_malloc (strlen (str) + 1);
      strcpy (fontpanel->input_string, str);
    }

  return fontpanel->input_string;
}

static void
gtk_dps_fontpanel_destroy (GtkObject * object)
{
  GPSFontList *buf1;
  GPSFontList *garbage1;
  TypefaceList *buf2;
  TypefaceList *garbage2;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_DPS_FONTPANEL (object));


  buf1 = dps_fonts;

  while (1)
    {
      if (!buf1)
	{
	  break;
	}
      garbage1 = buf1;

      buf2 = buf1->typeface;
      while (1)
	{
	  if (!buf2)
	    {
	      break;
	    }
	  garbage2 = buf2;

	  if (buf2->typeface)
	    g_free (buf2->typeface);

	  if (buf2->fontname)
	    g_free (buf2->fontname);
	  buf2 = buf2->next;
	  g_free (garbage2);
	}
      if (garbage1->family)
	{
	  g_free (garbage1->family);
	}
      buf1 = buf1->next;
      g_free (garbage1);
    }

  if (GTK_DPS_FONTPANEL (object)->input_string)
    g_free (GTK_DPS_FONTPANEL (object)->input_string);
}

static void
gtk_dps_fontpanel_realize (GtkWidget * widget)
{
  GtkDpsFontpanel *fontpanel;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_DPS_FONTPANEL (widget));

  fontpanel = GTK_DPS_FONTPANEL (widget);

  if (GTK_WIDGET_CLASS (dps_fontpanel_parent_class)->realize)
    (*GTK_WIDGET_CLASS (dps_fontpanel_parent_class)->realize) (widget);

  /* load fonts  -jet */
  load_fonts (fontpanel->dps_area, fontpanel->family_box);
}

static void
init_dpsarea (GtkDpsFontpanel * fontpanel, GtkWidget * parent)
{

#if HAVE_DPS_NXAGENT
  fprintf (stderr, "Set DPS NXAGENT to lauch automatically...");
  XDPSNXSetClientArg (XDPSNX_AUTO_LAUNCH, (void *) True);
  fprintf (stderr, "done\n");
#endif
  fontpanel->dps_area = gtk_dps_area_new ();
  gtk_dps_area_size (GTK_DPS_AREA (fontpanel->dps_area), AREA_SIZE_WIDTH, AREA_SIZE_HEIGHT);
  gtk_widget_set_events (fontpanel->dps_area, GDK_EXPOSURE_MASK);
  gtk_signal_connect (GTK_OBJECT (fontpanel->dps_area), "expose_event",
		      GTK_SIGNAL_FUNC (expose_handler), fontpanel);
  gtk_object_set_user_data (GTK_OBJECT (fontpanel->dps_area), fontpanel);
  gtk_container_add (GTK_CONTAINER (parent), fontpanel->dps_area);
  gtk_widget_show (fontpanel->dps_area);
}

static void
init_font_family (GtkDpsFontpanel * fontpanel, GtkWidget * parent)
{
  GtkWidget *main_box;
  GtkWidget *label;
  GtkWidget *frame;
  GtkWidget *win;

  main_box = gtk_vbox_new (FALSE, 3);
  gtk_box_pack_start (GTK_BOX (parent), main_box, FALSE, FALSE, 0);
  gtk_widget_show (main_box);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
  gtk_box_pack_start (GTK_BOX (main_box), frame, FALSE, FALSE, 0);
  gtk_widget_show (frame);

  label = gtk_label_new ("Family");
  gtk_container_add (GTK_CONTAINER (frame), label);
  gtk_widget_show (label);


  win = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (win),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_ALWAYS);
  gtk_widget_set_usize (win, LIST_WIDTH, LIST_HEIGHT);
  gtk_signal_connect (GTK_OBJECT (win), "button_press_event",
		      (GtkSignalFunc) change_family, fontpanel);
  gtk_box_pack_start (GTK_BOX (main_box), win, TRUE, TRUE, 0);
  gtk_widget_show (win);

  fontpanel->family_box = gtk_list_new ();
  gtk_list_set_selection_mode (GTK_LIST (fontpanel->family_box), GTK_SELECTION_BROWSE);
  gtk_container_add (GTK_CONTAINER (win), fontpanel->family_box);
  gtk_widget_show (fontpanel->family_box);
}

static void
init_font_face (GtkDpsFontpanel * fontpanel, GtkWidget * parent)
{
  GtkWidget *main_box;
  GtkWidget *label;
  GtkWidget *frame;
  GtkWidget *win;

  main_box = gtk_vbox_new (FALSE, 3);
  gtk_box_pack_start (GTK_BOX (parent), main_box, FALSE, FALSE, 0);
  gtk_widget_show (main_box);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
  gtk_box_pack_start (GTK_BOX (main_box), frame, FALSE, FALSE, 0);
  gtk_widget_show (frame);

  label = gtk_label_new ("Typeface");
  gtk_container_add (GTK_CONTAINER (frame), label);
  gtk_widget_show (label);


  win = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (win),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_ALWAYS);
  gtk_widget_set_usize (win, LIST_WIDTH, LIST_HEIGHT);
  gtk_signal_connect (GTK_OBJECT (win), "button_press_event",
		      (GtkSignalFunc) change_typeface, fontpanel);
  gtk_box_pack_start (GTK_BOX (main_box), win, TRUE, TRUE, 0);
  gtk_widget_show (win);

  fontpanel->face_box = gtk_list_new ();
  gtk_list_set_selection_mode (GTK_LIST (fontpanel->face_box), GTK_SELECTION_BROWSE);
  gtk_container_add (GTK_CONTAINER (win), fontpanel->face_box);
  gtk_widget_show (fontpanel->face_box);
}

static void
init_font_size (GtkDpsFontpanel * fontpanel, GtkWidget * parent)
{
  GtkWidget *main_box;
  GtkWidget *frame;
  GtkWidget *label;
  GtkWidget *win;
  GtkWidget *listbox;
  GtkWidget *list_item;
  int i;
  char *sizes[] =
  {"8", "10", "12", "14", "16", "24", "36", "48", "64", "128", "256"};
  int lots_of_sizes_entries = sizeof(sizes)/sizeof(char *);

  main_box = gtk_vbox_new (FALSE, 3);
  gtk_box_pack_start (GTK_BOX (parent), main_box, FALSE, FALSE, 0);
  gtk_widget_show (main_box);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
  gtk_box_pack_start (GTK_BOX (main_box), frame, FALSE, FALSE, 0);
  gtk_widget_show (frame);

  label = gtk_label_new ("Size");
  gtk_container_add (GTK_CONTAINER (frame), label);
  gtk_widget_show (label);


  fontpanel->size_entry = gtk_entry_new ();
  gtk_widget_set_usize (fontpanel->size_entry, 60, 20);
  gtk_box_pack_start (GTK_BOX (main_box), fontpanel->size_entry, FALSE, FALSE, 0);
  gtk_widget_show (fontpanel->size_entry);


  win = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (win),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_ALWAYS);
  gtk_signal_connect (GTK_OBJECT (win), "button_press_event",
		      (GtkSignalFunc) change_size, fontpanel);
  gtk_box_pack_start (GTK_BOX (main_box), win, TRUE, TRUE, 0);
  gtk_widget_show (win);

  listbox = gtk_list_new ();
  gtk_list_set_selection_mode (GTK_LIST (listbox), GTK_SELECTION_BROWSE);
  gtk_container_add (GTK_CONTAINER (win), listbox);
  gtk_widget_show (listbox);


  /* size */
  for (i = 0; i < lots_of_sizes_entries; i++)
    {
      list_item = gtk_list_item_new_with_label (sizes[i]);
      gtk_container_add (GTK_CONTAINER (listbox), list_item);
      gtk_object_set_user_data (GTK_OBJECT (list_item), sizes[i]);
      gtk_widget_show (list_item);
    }
}

static void
load_fonts (GtkWidget * widget, GtkWidget * box)
{
  char product_of_dps[255];

  gtk_widget_realize (widget);
  gtk_dps_area_begin (GTK_DPS_AREA (widget));
  {
    context = gtk_dps_get_current_raw_context ();
    PSWProduct (product_of_dps);
  }
  gtk_dps_area_flush (GTK_DPS_AREA (widget));
  gtk_dps_area_end (GTK_DPS_AREA (widget));

#if DEBUG
  fprintf (stderr, "DEBUG: product-> %s\n", product_of_dps);
#endif /* DEBUG */

  if (is_aladdin_product (product_of_dps))
    {
#if DEBUG
      fprintf (stderr, "DEBUG: is dgs\n");
#endif /* DEBUG */
      load_dgs_fonts (widget);
    }
  else
    {
#if DEBUG
      fprintf (stderr, "DEBUG: is dps\n");
#endif /* DEBUG */
      load_dps_fonts (widget);
    }
  load_font_list (box);
}

static void
load_dgs_fonts (GtkWidget * widget)
{
  gtk_dps_area_begin (GTK_DPS_AREA (widget));
  {
    context = gtk_dps_get_current_raw_context ();
    init_dgs_fonts (context);
  }
  gtk_dps_area_flush (GTK_DPS_AREA (widget));
  gtk_dps_area_end (GTK_DPS_AREA (widget));

  dps_fonts = init_gtkdps_font_list ();
}

static void
load_dps_fonts (GtkWidget * widget)
{
  EnumeratePSResourceFiles (NULL, NULL, PSResFontAFM, NULL,
			    init_dps_fonts, NULL);

  dps_fonts = init_gtkdps_font_list ();
}

static void
init_dgs_fonts (DPSContext ctxt)
{
  SList *list_tmp = NULL;
  char fontName[FILENAME_MAX + 1];
  char fileOrFont[FILENAME_MAX + 1];
  int noOfFonts;
  int i;
  char *name_tmp;

  DPSSetContext (ctxt);
  PSWFontNames (&noOfFonts);
  for (i = 0; i < noOfFonts; i++)
    {
      PSWGetFontsArray (fontName, fileOrFont);
      name_tmp = (char *) g_malloc (strlen (fontName) + 1);
      strcpy (name_tmp, fontName);

      if (!font_name_list)
	{
	  /* allocate */
	  font_name_list = (SList *) g_malloc (sizeof (SList));
	  list_tmp = font_name_list;
	}
      else
	{
	  /* allocate */
	  list_tmp->next = (SList *) g_malloc (sizeof (SList));
	  list_tmp = list_tmp->next;
	}
      list_tmp->next = NULL;
      list_tmp->data = name_tmp;
    }
  PScleartomark ();
}

static int
init_dps_fonts (char *resourceType, char *resourceName, char *resourceFile,
		char *nouse)
{
  static SList *list_tmp = NULL;
  char *name_tmp = NULL;	/* fullname */

  if (!resourceName)
    return 1;

  name_tmp = (char *) g_malloc (strlen (resourceName) + 1);
  strcpy (name_tmp, resourceName);

  if (!font_name_list)
    {
      font_name_list = (SList *) g_malloc (sizeof (SList));
      list_tmp = font_name_list;
    }
  else
    {
      /* allocate */
      list_tmp->next = (SList *) g_malloc (sizeof (SList));
      list_tmp = list_tmp->next;
    }

  list_tmp->next = NULL;
  list_tmp->data = name_tmp;

  return 0;
}

static GPSFontList *
init_gtkdps_font_list (void)
{
  GPSFontList *font_list = NULL;
  GPSFontList *fontl_tmp = NULL;
  TypefaceList *facel_tmp = NULL;
  SList *font_tmp;
  SList *garbage;
  char *fonts[1024];
  char *p;
  char buf[256];
  char *family_tmp;
  char *prev = NULL;
  int i;
  int index;
  int (*func) () = sort_by_name;

  /* sort */
  i = 0;
  font_tmp = font_name_list;
  while (1)
    {
      if (!font_tmp)
	{
	  break;
	}
      fonts[i++] = font_tmp->data;

      /* next */
      garbage = font_tmp;
      font_tmp = font_tmp->next;
      g_free (garbage);
    }
  font_name_list = NULL;
  index = i;
  qsort (fonts, index, sizeof (char *), func);

  /* alloc */
  for (i = 0; i < index; i++)
    {
      memset ((char *) buf, 0, 256);
      strcpy (buf, fonts[i]);
      p = strchr (buf, '-');
      if (p)
	{
	  *p = '\0';
	  family_tmp = (char *) g_malloc (strlen (buf) + 1);
	  strcpy (family_tmp, buf);
	}
      else
	{
	  family_tmp = (char *) g_malloc (strlen (buf) + 1);
	  strcpy (family_tmp, buf);
	}

      if (prev && strcmp (prev, family_tmp) == 0)
	{
	  g_free (family_tmp);
	  facel_tmp->next = (TypefaceList *) g_malloc (sizeof (TypefaceList));
	  facel_tmp = facel_tmp->next;
	  facel_tmp->next = NULL;
	  facel_tmp->fontname = fonts[i];
	  facel_tmp->typeface = NULL;

	  facel_tmp->typeface = get_typeface (facel_tmp->fontname);
	}
      else
	{
	  if (!font_list)
	    {
	      font_list = (GPSFontList *) g_malloc (sizeof (GPSFontList));
	      fontl_tmp = font_list;
	    }
	  else
	    {
	      fontl_tmp->next = (GPSFontList *) g_malloc (sizeof (GPSFontList));
	      fontl_tmp = fontl_tmp->next;
	    }
	  fontl_tmp->next = NULL;
	  fontl_tmp->family = family_tmp;
	  prev = family_tmp;

	  fontl_tmp->typeface = (TypefaceList *) g_malloc (sizeof (TypefaceList));
	  fontl_tmp->typeface->fontname = fonts[i];
	  fontl_tmp->typeface->typeface = NULL;
	  fontl_tmp->typeface->next = NULL;
	  facel_tmp = fontl_tmp->typeface;

	  facel_tmp->typeface = get_typeface (facel_tmp->fontname);
	}
    }

  return font_list;
}

static void
load_font_list (GtkWidget * widget)
{
  GPSFontList *buf;
  GtkWidget *list_item;

  buf = dps_fonts;

  while (1)
    {
      if (!buf)
	{
	  break;
	}
      list_item = gtk_list_item_new_with_label (buf->family);
      gtk_container_add (GTK_CONTAINER (widget), list_item);
      gtk_object_set_user_data (GTK_OBJECT (list_item), buf);
      gtk_widget_show (list_item);
      buf = buf->next;
    }

}


static void
change_family (GtkWidget * widget, GdkEventButton * event, gpointer data)
{
  GtkDpsFontpanel *fontpanel = data;
  GtkWidget *event_widget;
  GtkWidget *list_item;
  GPSFontList *family;
  TypefaceList *buf;
  static int flags;

  event_widget = gtk_get_event_widget ((GdkEvent *) event);
  family = (GPSFontList *) gtk_object_get_user_data (GTK_OBJECT (event_widget));
  if (!family)
    return;

  if (flags)
    {
      gtk_container_foreach (GTK_CONTAINER (fontpanel->face_box),
			     (GtkCallback) gtk_widget_destroy, NULL);
    }

  if (!family->typeface->typeface)
    {
      fontpanel->font_name = family->typeface->fontname;
    }
  else
    {
      fontpanel->font_name = NULL;
    }

  buf = family->typeface;
  while (1)
    {
      if (!buf)
	{
	  break;
	}

      if (buf->typeface)
	{
	  list_item = gtk_list_item_new_with_label (buf->typeface);
	}
      else
	{
	  list_item = gtk_list_item_new_with_label ("(void)");
	}
      gtk_container_add (GTK_CONTAINER (fontpanel->face_box), list_item);
      gtk_object_set_user_data (GTK_OBJECT (list_item), buf);
      gtk_widget_show (list_item);
      buf = buf->next;
    }
  flags = 1;
}

static void
change_typeface (GtkWidget * widget, GdkEventButton * event, gpointer data)
{
  GtkDpsFontpanel *fontpanel = data;
  GtkWidget *event_widget;
  TypefaceList *buf;

  event_widget = gtk_get_event_widget ((GdkEvent *) event);
  buf = (TypefaceList *) gtk_object_get_user_data (GTK_OBJECT (event_widget));
  if (!buf)
    return;

  GTK_DPS_FONTPANEL (fontpanel)->font_name = buf->fontname;
}

static void
change_size (GtkWidget * widget, GdkEventButton * event, gpointer data)
{
  GtkDpsFontpanel *fontpanel = data;
  GtkWidget *event_widget;
  char *buf;

  event_widget = gtk_get_event_widget ((GdkEvent *) event);
  buf = (char *) gtk_object_get_user_data (GTK_OBJECT (event_widget));
  if (!buf)
    return;

  gtk_entry_set_text (GTK_ENTRY (fontpanel->size_entry), buf);
}

static void
preview_font (GtkWidget * button_widget, void *user_data)
{
  char *name;
  char *str;
  int size;
  GtkDpsFontpanel *fontpanel = user_data;

  name = gtk_dps_fontpanel_get_fontname (fontpanel);
  size = gtk_dps_fontpanel_get_fontsize (fontpanel);
  str = gtk_dps_fontpanel_get_input (fontpanel);

#if DEBUG
  fprintf (stderr, "font %s : %d\n", fontpanel->font_name ? fontpanel->font_name : "<NULL>",
	   fontpanel->font_size);
  fprintf (stderr, "string %s\n",
	   fontpanel->input_string ? fontpanel->input_string : "<NULL>");
#endif /* DEBUG */
  sample_string_drawer_core (fontpanel->dps_area, name, size, str);
}

static void
sample_string_drawer_core (GtkWidget * widget, char *name, int size,
			   char *string)
{
  char *default_input_string = "sample";
  DPSContext local_context;

  if (!string)
    string = default_input_string;
  if (widget == NULL)
    return;

  gtk_dps_area_begin (GTK_DPS_AREA (widget));
  {
    local_context = gtk_dps_get_current_raw_context ();
    DPSsetrgbcolor (local_context, 1.0, 1.0, 1.0);
    DPSmoveto (local_context, -1000.0, -1000.0);
    DPSlineto (local_context, 1000.0, -1000.0);
    DPSlineto (local_context, 1000.0, 1000.0);
    DPSlineto (local_context, -1000.0, 1000.0);
    DPSlineto (local_context, -1000.0, -1000.0);
    DPSfill (local_context);
    /* jet's code 
       if (name == NULL) return ;
       if (size == 0) return ;
     */
    if (name && size > 0)
      {
	PSWShowSampleString (local_context, name, size, string);
      }

  }
  gtk_dps_area_flush (GTK_DPS_AREA (widget));
  gtk_dps_area_end (GTK_DPS_AREA (widget));
}

/* return 1 if dps server is a product of Aladdin(dgs) 
   return 0 if dps server is a product of Adobe(dps) */
static int
is_aladdin_product (char *product_name)
{
  const char *aladdin_product_name = "Aladdin Ghostscript";
  int aladdin_product_name_prefix_length = strlen (aladdin_product_name);
  if (!strncmp (aladdin_product_name, product_name,
		aladdin_product_name_prefix_length))
    {
      /* is dgs */
      return 1;
    }
  else
    {
      /* is dps */
      return 0;
    }
}

static char *
get_typeface (char *pathname)
{
  char buf[256];
  char *typeface;
  char *p;

  strcpy (buf, pathname);
  p = strchr (buf, '-');
  if (p)
    {
      p += 1;
      typeface = (char *) g_malloc (strlen (p) + 1);
      strcpy (typeface, p);
    }
  else
    {
      typeface = NULL;
    }

  return typeface;
}

static int
sort_by_name (const char **file1, const char **file2)
{
  return strcmp (*file1, *file2);
}

static void
expose_handler (GtkWidget * widget, GdkEventButton * event)
{
  GtkDpsFontpanel *fontpanel;
  GtkWidget *event_widget;

  event_widget = gtk_get_event_widget ((GdkEvent *) event);
  fontpanel = (GtkDpsFontpanel *) gtk_object_get_user_data (GTK_OBJECT (event_widget));
  if (!fontpanel)
    return;

  sample_string_drawer_core (fontpanel->dps_area, fontpanel->font_name,
			     fontpanel->font_size, fontpanel->input_string);
}
