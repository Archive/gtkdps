/* fonts.c generated from fonts.psw
   by unix pswrap V1.009  Wed Apr 19 17:50:24 PDT 1989
 */

#include <DPS/dpsfriends.h>
#include <string.h>

#line 1 "fonts.psw"
/*
   fonts.psw

   Support functions to determine the installed fonts.

   Copyright (C) 1996 Free Software Foundation, Inc.

   Author: Ovidiu Predescu <ovidiu@bx.logicnet.ro>
   Date: February 1997
   
   This file is part of the GNUstep GUI X/DPS Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
   
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*
 *  PSWShowSampleString added by Masatake YAMATO<masata-y@is.aist-nara.ac.jp>.
 *  This functions for fontpanel widget of gtkDPS.
 */

#include <DPS/dpsclient.h>

/* PSWFontNames() puts on the PS stack a sequence of two elements pairs and
   returns the number of the total elements. Each pair of elements consists
   from the name of the font and either the filename where the font is defined
   or the name of the font whose alias is. To get each pair of elements call
   the PSWGetNextFont() function.

  Note: This procedure depends on DGS. The function to be used on a DPS system
    conforming to red book should use the following statements to find out the
    font names:

    /fonts [
      (%font%*) {100 string copy} 100 string filenameforall
    ] def

    The mapping between font names and file name is DPS dependent. On NeXTSTEP
    for example (and on systems conforming to red book), you can find out the
    filename where a font is defined like below:

    /Font /Category findresource
    begin
      /Times-Roman 1024 string ResourceFileName
    end

    This pushes on the stack %font%Times-Roman. The program should search in
    the standard places a directory called `Times-Roman.font' to determine the
    definition of the font.

    Maybe we should place in Postscript all the DPS dependencies and present to
    the program the same interface.
*/
#line 74 "fonts.c"
void PSWFontNames(int *noOfFonts)
{
  typedef struct {
    unsigned char tokenType;
    unsigned char topLevelCount;
    unsigned short nBytes;

    DPSBinObjGeneric obj0;
    DPSBinObjGeneric obj1;
    DPSBinObjGeneric obj2;
    DPSBinObjGeneric obj3;
    DPSBinObjGeneric obj4;
    DPSBinObjGeneric obj5;
    DPSBinObjGeneric obj6;
    DPSBinObjGeneric obj7;
    DPSBinObjGeneric obj8;
    DPSBinObjGeneric obj9;
    DPSBinObjGeneric obj10;
    DPSBinObjGeneric obj11;
    DPSBinObjGeneric obj12;
    DPSBinObjGeneric obj13;
    DPSBinObjGeneric obj14;
    DPSBinObjGeneric obj15;
    DPSBinObjGeneric obj16;
    DPSBinObjGeneric obj17;
    DPSBinObjGeneric obj18;
    DPSBinObjGeneric obj19;
    DPSBinObjGeneric obj20;
    DPSBinObjGeneric obj21;
    DPSBinObjGeneric obj22;
    DPSBinObjGeneric obj23;
    DPSBinObjGeneric obj24;
    DPSBinObjGeneric obj25;
    DPSBinObjGeneric obj26;
    DPSBinObjGeneric obj27;
    DPSBinObjGeneric obj28;
    DPSBinObjGeneric obj29;
    DPSBinObjGeneric obj30;
    DPSBinObjGeneric obj31;
    DPSBinObjGeneric obj32;
    DPSBinObjGeneric obj33;
    DPSBinObjGeneric obj34;
    DPSBinObjGeneric obj35;
    DPSBinObjGeneric obj36;
    DPSBinObjGeneric obj37;
    DPSBinObjGeneric obj38;
    DPSBinObjGeneric obj39;
    DPSBinObjGeneric obj40;
    DPSBinObjGeneric obj41;
    DPSBinObjGeneric obj42;
    DPSBinObjGeneric obj43;
    DPSBinObjGeneric obj44;
    DPSBinObjGeneric obj45;
    DPSBinObjGeneric obj46;
    } _dpsQ;
  static const _dpsQ _dpsStat = {
    DPS_DEF_TOKENTYPE, 19, 380,
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 318},	/* mark */
    {DPS_LITERAL|DPS_NAME, 0, 0, 0},	/* temp */
    {DPS_LITERAL|DPS_INT, 0, 0, 1024},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 165},	/* string */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 51},	/* def */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 182},	/* userdict */
    {DPS_LITERAL|DPS_NAME, 0, 0, 0},	/* Fontmap */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 75},	/* get */
    {DPS_EXEC|DPS_ARRAY, 0, 26, 152},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 73},	/* forall */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 27},	/* counttomark */
    {DPS_LITERAL|DPS_INT, 0, 0, 2},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 82},	/* idiv */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_LITERAL|DPS_INT, 0, 0, 1},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 70},	/* flush */
    {DPS_LITERAL|DPS_NAME, 0, 0, 0},	/* filename */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 62},	/* exch */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 56},	/* dup */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 175},	/* type */
    {DPS_LITERAL|DPS_NAME, 0, 0, 0},	/* arraytype */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 61},	/* eq */
    {DPS_EXEC|DPS_ARRAY, 0, 2, 360},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 84},	/* if */
    {DPS_EXEC|DPS_NAME, 0, 0, 0},	/* temp */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 49},	/* cvs */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 56},	/* dup */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 98},	/* length */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 165},	/* string */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 25},	/* copy */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 51},	/* def */
    {DPS_LITERAL|DPS_NAME, 0, 0, 0},	/* fontname */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 62},	/* exch */
    {DPS_EXEC|DPS_NAME, 0, 0, 0},	/* temp */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 49},	/* cvs */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 56},	/* dup */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 98},	/* length */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 165},	/* string */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 25},	/* copy */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 51},	/* def */
    {DPS_EXEC|DPS_NAME, 0, 0, 0},	/* fontname */
    {DPS_EXEC|DPS_NAME, 0, 0, 0},	/* filename */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 2},	/* aload */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 117},	/* pop */
    }; /* _dpsQ */
  _dpsQ _dpsF;	/* local copy  */
  register DPSContext _dpsCurCtxt = DPSPrivCurrentContext();
  register DPSBinObjRec *_dpsP = (DPSBinObjRec *)&_dpsF.obj0;
  static int _dpsCodes[9] = {-1};
  DPSResultsRec _dpsR[1];
  static const DPSResultsRec _dpsRstat[] = {
    { dps_tInt, -1 },
    };
    _dpsR[0] = _dpsRstat[0];
    _dpsR[0].value = (char *)noOfFonts;

  {
if (_dpsCodes[0] < 0) {
    static const char * const _dps_names[] = {
	"temp",
	(char *) 0 ,
	(char *) 0 ,
	"Fontmap",
	"filename",
	(char *) 0 ,
	"arraytype",
	"fontname",
	(char *) 0 };
    int *_dps_nameVals[9];
    _dps_nameVals[0] = &_dpsCodes[0];
    _dps_nameVals[1] = &_dpsCodes[1];
    _dps_nameVals[2] = &_dpsCodes[2];
    _dps_nameVals[3] = &_dpsCodes[3];
    _dps_nameVals[4] = &_dpsCodes[4];
    _dps_nameVals[5] = &_dpsCodes[5];
    _dps_nameVals[6] = &_dpsCodes[6];
    _dps_nameVals[7] = &_dpsCodes[7];
    _dps_nameVals[8] = &_dpsCodes[8];

    DPSMapNames(_dpsCurCtxt, 9, (char **) _dps_names, _dps_nameVals);
    }
  }

  _dpsF = _dpsStat;	/* assign automatic variable */

  _dpsP[1].val.nameVal = _dpsCodes[0];
  _dpsP[36].val.nameVal = _dpsCodes[1];
  _dpsP[27].val.nameVal = _dpsCodes[2];
  _dpsP[6].val.nameVal = _dpsCodes[3];
  _dpsP[19].val.nameVal = _dpsCodes[4];
  _dpsP[44].val.nameVal = _dpsCodes[5];
  _dpsP[23].val.nameVal = _dpsCodes[6];
  _dpsP[34].val.nameVal = _dpsCodes[7];
  _dpsP[43].val.nameVal = _dpsCodes[8];
  DPSSetResultTable(_dpsCurCtxt, _dpsR, 1);
  DPSBinObjSeqWrite(_dpsCurCtxt,(char *) &_dpsF,380);
  DPSAwaitReturnValues(_dpsCurCtxt);
}
#line 81 "fonts.psw"

#line 235 "fonts.c"
void PSWGetFontsArray(char *fontname, char *fileOrFont)
{
  typedef struct {
    unsigned char tokenType;
    unsigned char topLevelCount;
    unsigned short nBytes;

    DPSBinObjGeneric obj0;
    DPSBinObjGeneric obj1;
    DPSBinObjGeneric obj2;
    DPSBinObjGeneric obj3;
    DPSBinObjGeneric obj4;
    DPSBinObjGeneric obj5;
    DPSBinObjGeneric obj6;
    DPSBinObjGeneric obj7;
    } _dpsQ;
  static const _dpsQ _dpsF = {
    DPS_DEF_TOKENTYPE, 8, 68,
    {DPS_LITERAL|DPS_INT, 0, 0, 1},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_LITERAL|DPS_INT, 0, 0, 2},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 70},	/* flush */
    }; /* _dpsQ */
  register DPSContext _dpsCurCtxt = DPSPrivCurrentContext();
  DPSResultsRec _dpsR[2];
  static const DPSResultsRec _dpsRstat[] = {
    { dps_tChar, -1 },
    { dps_tChar, -1 },
    };
    _dpsR[0] = _dpsRstat[0];
    _dpsR[0].count = -1;
    _dpsR[0].value = (char *)fontname;
    _dpsR[1] = _dpsRstat[1];
    _dpsR[1].count = -1;
    _dpsR[1].value = (char *)fileOrFont;

  DPSSetResultTable(_dpsCurCtxt, _dpsR, 2);
  DPSBinObjSeqWrite(_dpsCurCtxt,(char *) &_dpsF,68);
  DPSAwaitReturnValues(_dpsCurCtxt);
}
#line 85 "fonts.psw"



/* PSWCompleteFilename() returns the complete path to the filename passed as
   argument. The file is searched in the standard DGS places. */
#line 286 "fonts.c"
void PSWCompleteFilename(const char *filename, int *found, char *completePath)
{
  typedef struct {
    unsigned char tokenType;
    unsigned char sizeFlag;
    unsigned short topLevelCount;
    unsigned int nBytes;

    DPSBinObjGeneric obj0;
    DPSBinObjGeneric obj1;
    DPSBinObjGeneric obj2;
    DPSBinObjGeneric obj3;
    DPSBinObjGeneric obj4;
    DPSBinObjGeneric obj5;
    DPSBinObjGeneric obj6;
    DPSBinObjGeneric obj7;
    DPSBinObjGeneric obj8;
    DPSBinObjGeneric obj9;
    DPSBinObjGeneric obj10;
    DPSBinObjGeneric obj11;
    DPSBinObjGeneric obj12;
    DPSBinObjGeneric obj13;
    DPSBinObjGeneric obj14;
    DPSBinObjGeneric obj15;
    DPSBinObjGeneric obj16;
    DPSBinObjGeneric obj17;
    DPSBinObjGeneric obj18;
    } _dpsQ;
  static const _dpsQ _dpsStat = {
    DPS_DEF_TOKENTYPE, 0, 9, 160,
    {DPS_LITERAL|DPS_STRING, 0, 0, 152},	/* param filename */
    {DPS_EXEC|DPS_NAME, 0, 0, 0},	/* findlibfile */
    {DPS_EXEC|DPS_ARRAY, 0, 6, 104},
    {DPS_EXEC|DPS_ARRAY, 0, 4, 72},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 85},	/* ifelse */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_LITERAL|DPS_INT, 0, 0, 2},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 70},	/* flush */
    {DPS_LITERAL|DPS_BOOL, 0, 0, 0},
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 117},	/* pop */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 266},	/* closefile */
    {DPS_LITERAL|DPS_INT, 0, 0, 1},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_LITERAL|DPS_BOOL, 0, 0, 1},
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    }; /* _dpsQ */
  _dpsQ _dpsF;	/* local copy  */
  register DPSContext _dpsCurCtxt = DPSPrivCurrentContext();
  register DPSBinObjRec *_dpsP = (DPSBinObjRec *)&_dpsF.obj0;
  static int _dpsCodes[1] = {-1};
  register int _dps_offset = 152;
  DPSResultsRec _dpsR[2];
  static const DPSResultsRec _dpsRstat[] = {
    { dps_tBoolean, -1 },
    { dps_tChar, -1 },
    };
    _dpsR[0] = _dpsRstat[0];
    _dpsR[0].value = (char *)found;
    _dpsR[1] = _dpsRstat[1];
    _dpsR[1].count = -1;
    _dpsR[1].value = (char *)completePath;

  {
if (_dpsCodes[0] < 0) {
    static const char * const _dps_names[] = {
	"findlibfile"};
    int *_dps_nameVals[1];
    _dps_nameVals[0] = &_dpsCodes[0];

    DPSMapNames(_dpsCurCtxt, 1, (char **) _dps_names, _dps_nameVals);
    }
  }

  _dpsF = _dpsStat;	/* assign automatic variable */

  _dpsP[0].length = strlen(filename);
  _dpsP[1].val.nameVal = _dpsCodes[0];
  _dpsP[0].val.stringVal = _dps_offset;
  _dps_offset += _dpsP[0].length;

  _dpsF.nBytes = _dps_offset+8;
  DPSSetResultTable(_dpsCurCtxt, _dpsR, 2);
  DPSBinObjSeqWrite(_dpsCurCtxt,(char *) &_dpsF,160);
  DPSWriteStringChars(_dpsCurCtxt, (char *)filename, _dpsP[0].length);
  DPSAwaitReturnValues(_dpsCurCtxt);
}
#line 93 "fonts.psw"

#line 379 "fonts.c"
void PSWProduct(char *productname)
{
  typedef struct {
    unsigned char tokenType;
    unsigned char topLevelCount;
    unsigned short nBytes;

    DPSBinObjGeneric obj0;
    DPSBinObjGeneric obj1;
    DPSBinObjGeneric obj2;
    DPSBinObjGeneric obj3;
    DPSBinObjGeneric obj4;
    DPSBinObjGeneric obj5;
    DPSBinObjGeneric obj6;
    } _dpsQ;
  static const _dpsQ _dpsStat = {
    DPS_DEF_TOKENTYPE, 7, 60,
    {DPS_EXEC|DPS_NAME, 0, 0, 0},	/* product */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_LITERAL|DPS_INT, 0, 0, 1},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 70},	/* flush */
    }; /* _dpsQ */
  _dpsQ _dpsF;	/* local copy  */
  register DPSContext _dpsCurCtxt = DPSPrivCurrentContext();
  register DPSBinObjRec *_dpsP = (DPSBinObjRec *)&_dpsF.obj0;
  static int _dpsCodes[1] = {-1};
  DPSResultsRec _dpsR[1];
  static const DPSResultsRec _dpsRstat[] = {
    { dps_tChar, -1 },
    };
    _dpsR[0] = _dpsRstat[0];
    _dpsR[0].count = -1;
    _dpsR[0].value = (char *)productname;

  {
if (_dpsCodes[0] < 0) {
    static const char * const _dps_names[] = {
	"product"};
    int *_dps_nameVals[1];
    _dps_nameVals[0] = &_dpsCodes[0];

    DPSMapNames(_dpsCurCtxt, 1, (char **) _dps_names, _dps_nameVals);
    }
  }

  _dpsF = _dpsStat;	/* assign automatic variable */

  _dpsP[0].val.nameVal = _dpsCodes[0];
  DPSSetResultTable(_dpsCurCtxt, _dpsR, 1);
  DPSBinObjSeqWrite(_dpsCurCtxt,(char *) &_dpsF,60);
  DPSAwaitReturnValues(_dpsCurCtxt);
}
#line 98 "fonts.psw"

#line 437 "fonts.c"
void PSWGetFontAndType(char *fontname, int *fonttype)
{
  typedef struct {
    unsigned char tokenType;
    unsigned char topLevelCount;
    unsigned short nBytes;

    DPSBinObjGeneric obj0;
    DPSBinObjGeneric obj1;
    DPSBinObjGeneric obj2;
    DPSBinObjGeneric obj3;
    DPSBinObjGeneric obj4;
    DPSBinObjGeneric obj5;
    DPSBinObjGeneric obj6;
    DPSBinObjGeneric obj7;
    } _dpsQ;
  static const _dpsQ _dpsF = {
    DPS_DEF_TOKENTYPE, 8, 68,
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_LITERAL|DPS_INT, 0, 0, 1},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_LITERAL|DPS_INT, 0, 0, 2},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 70},	/* flush */
    }; /* _dpsQ */
  register DPSContext _dpsCurCtxt = DPSPrivCurrentContext();
  DPSResultsRec _dpsR[2];
  static const DPSResultsRec _dpsRstat[] = {
    { dps_tChar, -1 },
    { dps_tInt, -1 },
    };
    _dpsR[0] = _dpsRstat[0];
    _dpsR[0].count = -1;
    _dpsR[0].value = (char *)fontname;
    _dpsR[1] = _dpsRstat[1];
    _dpsR[1].value = (char *)fonttype;

  DPSSetResultTable(_dpsCurCtxt, _dpsR, 2);
  DPSBinObjSeqWrite(_dpsCurCtxt,(char *) &_dpsF,68);
  DPSAwaitReturnValues(_dpsCurCtxt);
}
#line 103 "fonts.psw"



/* Set the current font */
#line 486 "fonts.c"
void PSWSetFont(const char *font, const float fontMatrix[])
{
  typedef struct {
    unsigned char tokenType;
    unsigned char sizeFlag;
    unsigned short topLevelCount;
    unsigned int nBytes;

    DPSBinObjGeneric obj0;
    DPSBinObjGeneric obj1;
    DPSBinObjGeneric obj2;
    DPSBinObjGeneric obj3;
    DPSBinObjGeneric obj4;
    } _dpsQ;
  static const _dpsQ _dpsStat = {
    DPS_DEF_TOKENTYPE, 0, 5, 96,
    {DPS_LITERAL|DPS_STRING, 0, 0, 88},	/* param font */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 67},	/* findfont */
    {DPS_LITERAL|DPS_ARRAY, 0, 6, 40},	/* param[const]: fontMatrix */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 103},	/* makefont */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 149},	/* setfont */
    }; /* _dpsQ */
  _dpsQ _dpsF;	/* local copy  */
  register DPSContext _dpsCurCtxt = DPSPrivCurrentContext();
  register DPSBinObjRec *_dpsP = (DPSBinObjRec *)&_dpsF.obj0;
  register int _dps_offset = 40;
  _dpsF = _dpsStat;	/* assign automatic variable */

  _dpsP[0].length = strlen(font);
  _dpsP[2].val.arrayVal = _dps_offset;
  _dps_offset += 6 * sizeof(DPSBinObjGeneric);
  _dpsP[0].val.stringVal = _dps_offset;
  _dps_offset += _dpsP[0].length;

  _dpsF.nBytes = _dps_offset+8;
  DPSBinObjSeqWrite(_dpsCurCtxt,(char *) &_dpsF,48);
  DPSWriteTypedObjectArray(_dpsCurCtxt, dps_tFloat, (char *)fontMatrix, 6);
  DPSWriteStringChars(_dpsCurCtxt, (char *)font, _dpsP[0].length);
  DPSSYNCHOOK(_dpsCurCtxt)
}
#line 109 "fonts.psw"



/* Get the font type */
#line 532 "fonts.c"
void PSWGetFontType(const char *font, int *fontType)
{
  typedef struct {
    unsigned char tokenType;
    unsigned char sizeFlag;
    unsigned short topLevelCount;
    unsigned int nBytes;

    DPSBinObjGeneric obj0;
    DPSBinObjGeneric obj1;
    DPSBinObjGeneric obj2;
    DPSBinObjGeneric obj3;
    DPSBinObjGeneric obj4;
    DPSBinObjGeneric obj5;
    DPSBinObjGeneric obj6;
    DPSBinObjGeneric obj7;
    DPSBinObjGeneric obj8;
    DPSBinObjGeneric obj9;
    } _dpsQ;
  static const _dpsQ _dpsStat = {
    DPS_DEF_TOKENTYPE, 0, 10, 88,
    {DPS_LITERAL|DPS_STRING, 0, 0, 80},	/* param font */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 67},	/* findfont */
    {DPS_LITERAL|DPS_NAME, 0, 0, 0},	/* FontType */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 75},	/* get */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_LITERAL|DPS_INT, 0, 0, 1},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 119},	/* printobject */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 70},	/* flush */
    }; /* _dpsQ */
  _dpsQ _dpsF;	/* local copy  */
  register DPSContext _dpsCurCtxt = DPSPrivCurrentContext();
  register DPSBinObjRec *_dpsP = (DPSBinObjRec *)&_dpsF.obj0;
  static int _dpsCodes[1] = {-1};
  register int _dps_offset = 80;
  DPSResultsRec _dpsR[1];
  static const DPSResultsRec _dpsRstat[] = {
    { dps_tInt, -1 },
    };
    _dpsR[0] = _dpsRstat[0];
    _dpsR[0].value = (char *)fontType;

  {
if (_dpsCodes[0] < 0) {
    static const char * const _dps_names[] = {
	"FontType"};
    int *_dps_nameVals[1];
    _dps_nameVals[0] = &_dpsCodes[0];

    DPSMapNames(_dpsCurCtxt, 1, (char **) _dps_names, _dps_nameVals);
    }
  }

  _dpsF = _dpsStat;	/* assign automatic variable */

  _dpsP[0].length = strlen(font);
  _dpsP[2].val.nameVal = _dpsCodes[0];
  _dpsP[0].val.stringVal = _dps_offset;
  _dps_offset += _dpsP[0].length;

  _dpsF.nBytes = _dps_offset+8;
  DPSSetResultTable(_dpsCurCtxt, _dpsR, 1);
  DPSBinObjSeqWrite(_dpsCurCtxt,(char *) &_dpsF,88);
  DPSWriteStringChars(_dpsCurCtxt, (char *)font, _dpsP[0].length);
  DPSAwaitReturnValues(_dpsCurCtxt);
}
#line 115 "fonts.psw"

#line 603 "fonts.c"
void PSWShowSampleString(DPSContext context, const char *font, int size, const char *sample)
{
  typedef struct {
    unsigned char tokenType;
    unsigned char sizeFlag;
    unsigned short topLevelCount;
    unsigned int nBytes;

    DPSBinObjGeneric obj0;
    DPSBinObjGeneric obj1;
    DPSBinObjGeneric obj2;
    DPSBinObjGeneric obj3;
    DPSBinObjGeneric obj4;
    DPSBinObjGeneric obj5;
    DPSBinObjGeneric obj6;
    DPSBinObjGeneric obj7;
    DPSBinObjGeneric obj8;
    DPSBinObjGeneric obj9;
    DPSBinObjGeneric obj10;
    DPSBinObjGeneric obj11;
    DPSBinObjGeneric obj12;
    DPSBinObjGeneric obj13;
    DPSBinObjGeneric obj14;
    DPSBinObjGeneric obj15;
    } _dpsQ;
  static const _dpsQ _dpsStat = {
    DPS_DEF_TOKENTYPE, 0, 16, 136,
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 78},	/* gsave */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 157},	/* setrgbcolor */
    {DPS_LITERAL|DPS_NAME, 0, 0, 128},	/* param font */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 67},	/* findfont */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},	/* param: size */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 140},	/* scalefont */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 149},	/* setfont */
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_LITERAL|DPS_INT, 0, 0, 0},
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 107},	/* moveto */
    {DPS_LITERAL|DPS_STRING, 0, 0, 128},	/* param sample */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 160},	/* show */
    {DPS_EXEC|DPS_NAME, 0, DPSSYSNAME, 77},	/* grestore */
    }; /* _dpsQ */
  _dpsQ _dpsF;	/* local copy  */
  register DPSBinObjRec *_dpsP = (DPSBinObjRec *)&_dpsF.obj0;
  register int _dps_offset = 128;
  _dpsF = _dpsStat;	/* assign automatic variable */

  _dpsP[5].length = strlen(font);
  _dpsP[7].val.integerVal = size;
  _dpsP[13].length = strlen(sample);
  _dpsP[13].val.stringVal = _dps_offset;
  _dps_offset += _dpsP[13].length;
  _dpsP[5].val.stringVal = _dps_offset;
  _dps_offset += _dpsP[5].length;

  _dpsF.nBytes = _dps_offset+8;
  DPSBinObjSeqWrite(context,(char *) &_dpsF,136);
  DPSWriteStringChars(context, (char *)sample, _dpsP[13].length);
  DPSWriteStringChars(context, (char *)font, _dpsP[5].length);
  DPSSYNCHOOK(context)
}
#line 125 "fonts.psw"

