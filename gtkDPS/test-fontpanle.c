/*
 * gtkDPS font panel test program by Hideki FUJIMOTO
 * 
 */ 

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include "gtkdpsfontpanel.h"
static void ok_handler (GtkWidget * widget, gpointer data);

static void
init_gui (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *main_box;
  GtkWidget *fpanel;

  /* Initialize Gtk */
  gtk_init (&argc, &argv);

  /* Create the main window */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_wmclass (GTK_WINDOW (window), "font panel", "Font panel");
  gtk_window_set_title (GTK_WINDOW (window), "Font panel");
  gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, FALSE);
  gtk_widget_set_name (window, "main window");
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_exit), NULL);
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (gtk_exit), NULL);

  /* Create a container to hold everything */
  main_box = gtk_vbox_new (FALSE, 6);
  gtk_container_add (GTK_CONTAINER (window), main_box);
  gtk_container_border_width (GTK_CONTAINER (main_box), 8);
  gtk_widget_show (main_box);

  /* Init font panel */
  fpanel = gtk_dps_fontpanel_new ();
  gtk_box_pack_start (GTK_BOX (main_box), fpanel, TRUE, TRUE, 0);
  gtk_widget_show (fpanel);

  gtk_signal_connect (GTK_OBJECT (GTK_DPS_FONTPANEL (fpanel)->close_button),
		      "clicked", GTK_SIGNAL_FUNC (gtk_exit), NULL);

  gtk_signal_connect (GTK_OBJECT (GTK_DPS_FONTPANEL (fpanel)->ok_button),
		      "clicked", GTK_SIGNAL_FUNC (ok_handler), fpanel);
  /* Show all */
  gtk_widget_show (window);
}

static void
ok_handler (GtkWidget * widget, gpointer data)
{
  char *fontname;
  char *input;
  int size;
  GtkDpsFontpanel *fp = data;

  fontname = (char *) gtk_dps_fontpanel_get_fontname (GTK_DPS_FONTPANEL (fp));
  printf ("#%s\n", fontname);
  size = gtk_dps_fontpanel_get_fontsize (GTK_DPS_FONTPANEL (fp));
  printf ("#%d\n", size);
  input = (char *) gtk_dps_fontpanel_get_input (GTK_DPS_FONTPANEL (fp));
  printf ("#%s\n", input);
}

int
main (int argc, char *argv[])
{
  init_gui (argc, argv);
  gtk_main ();

  return 0;
}
