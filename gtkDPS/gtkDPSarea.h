/* gtkDPSarea.h
 * Copyright (C) 1997, 1998 GYVE Development Team
 *
 * Author:  Terumoto 'tel' HAYAKAWA <hayakawa@cv.cs.ritsumei.ac.jp>
 * Created: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GTK__DPS_AREA_H
#define GTK__DPS_AREA_H 


#include <gdk/gdk.h>
#include <gtkDPS/gtkDPSwidget.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_DPS_AREA(obj)          GTK_CHECK_CAST (obj, gtk_dps_area_get_type (), GtkDPSArea)
#define GTK_DPS_AREA_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_dps_area_get_type (), GtkDPSAreaClass)
#define GTK_IS_DPS_AREA(obj)       GTK_CHECK_TYPE (obj, gtk_dps_area_get_type ())


typedef struct _GtkDPSArea       GtkDPSArea;
typedef struct _GtkDPSAreaClass  GtkDPSAreaClass;

struct _GtkDPSArea
{
  GtkDPSWidget dps_widget;
};

struct _GtkDPSAreaClass
{
  GtkDPSWidgetClass parent_class;
};

guint      gtk_dps_area_get_type       (void);
GtkWidget* gtk_dps_area_new        (void);

void       gtk_dps_area_size           (GtkDPSArea *, gint, gint);

void       gtk_dps_area_begin             (GtkDPSArea *);
void       gtk_dps_area_end               (GtkDPSArea *);
void       gtk_dps_area_flush             (GtkDPSArea *);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* Not def: GTK__DPS_AREA_H */

