/*
 * gtkDPS single context test program by Hideki FUJIMOTO
 */  
#include <gtk/gtk.h>
#include "gtkDPS.h"
#include <DPS/psops.h>
#include <DPS/dpsfriends.h>
#include <DPS/dpsXclient.h>
#include <DPS/dpsops.h>

#ifdef HAVE_DPS_NXAGENT    
#include <DPS/dpsNXargs.h>
#endif

#ifdef _NO_PROTO
#define ARGCAST int
#else
#define ARGCAST void *
#endif

static void init_dialog(void);
static void dialog_close_callback (void);
static void draw_circle1 (void);
static void draw_circle2 (void);
static void draw_line1 (void);
static void draw_line2 (void);

static GtkWidget *DPS_area1;
static GtkWidget *DPS_area2;
/* 
 * This is callback function. 
 * The data arguments are ignored in this example..
 * More on callbacks below. 
 */
static void
dialog_close_callback (void)
{
	gtk_main_quit();
}

/*
 * This callback function is to draw a circle. 
 */
static void
draw_circle1 (void)
{
	DPSContext ctxt1;
	float i;
	
	gtk_dps_area_begin(GTK_DPS_AREA (DPS_area1));
	{
		ctxt1 = gtk_dps_get_current_raw_context();
		for (i = 1.0; i > 0.0; i = i - 0.01) {
			DPSnewpath(ctxt1);
			DPSarc(ctxt1, 150.0, 200.0, i * 50.0, 0.0, 360.0);
			DPSsetrgbcolor(ctxt1, 1.0, i, 1.0);
			DPSfill(ctxt1);
		}
	}
	gtk_dps_area_flush(GTK_DPS_AREA (DPS_area1));
	gtk_dps_area_end(GTK_DPS_AREA (DPS_area1));
}

static void
draw_circle2 (void)
{
	DPSContext ctxt2;
	float i;
	
	gtk_dps_area_begin(GTK_DPS_AREA (DPS_area2));
	{
		ctxt2 = gtk_dps_get_current_raw_context();
		for (i = 1.0; i > 0.0; i = i - 0.01) {
			DPSnewpath(ctxt2);
			DPSarc(ctxt2, 100.0, 150.0, i * 50.0, 0.0, 360.0);
			DPSsetrgbcolor(ctxt2, i, 1.0, 1.0);
			DPSfill(ctxt2);
		}
	}
	gtk_dps_area_flush(GTK_DPS_AREA (DPS_area2));
	gtk_dps_area_end(GTK_DPS_AREA (DPS_area2));
}


/*
 * This callback function is to draw a line.
 */
static void
draw_line1 (void)
{
	static DPSContext ctxt1;

	gtk_dps_area_begin(GTK_DPS_AREA (DPS_area1));
	{
		ctxt1 = gtk_dps_get_current_raw_context();
		DPSnewpath(ctxt1);
		DPSmoveto(ctxt1, 0.0, 0.0);
		DPSsetrgbcolor(ctxt1, 0.0, 0.0, 1.0);
		DPSlineto(ctxt1, 400.0, 400.0);
		DPSstroke(ctxt1);
	}
	gtk_dps_area_flush(GTK_DPS_AREA (DPS_area1));
	gtk_dps_area_end(GTK_DPS_AREA (DPS_area1));
}

static void
draw_line2 (void)
{
	static DPSContext ctxt2;

	gtk_dps_area_begin(GTK_DPS_AREA (DPS_area2));
	{
		ctxt2 = gtk_dps_get_current_raw_context();
		DPSnewpath(ctxt2);
		DPSmoveto(ctxt2, 0.0, 0.0);
		DPSsetrgbcolor(ctxt2, 0.0, 1.0, 1.0);
		DPSlineto(ctxt2, 200.0, 500.0);
		DPSstroke(ctxt2);
	}
	gtk_dps_area_flush(GTK_DPS_AREA (DPS_area2));
	gtk_dps_area_end(GTK_DPS_AREA (DPS_area2));
}

static void
init_dialog(void)
{
	GtkWidget *dialog;
	GtkWidget *close_button;
	GtkWidget *button;

	/* create a new dialog */
	dialog = gtk_dialog_new();
	
	
	/* create a new button with the label "Close". */
	close_button = gtk_button_new_with_label("Close");
	gtk_signal_connect(GTK_OBJECT(close_button), "clicked",
			   (GtkSignalFunc)dialog_close_callback, NULL);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
			   close_button, TRUE, TRUE, 0);
	gtk_widget_show(close_button);
	
	/* line 1 */
	button = gtk_button_new_with_label("line 1");
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
			   (GtkSignalFunc)draw_line1, NULL);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
			   button, TRUE, TRUE, 0);
	gtk_widget_show(button);

	/* line 2 */
	button = gtk_button_new_with_label("line 2");
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
			   (GtkSignalFunc)draw_line2, NULL);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
			   button, TRUE, TRUE, 0);
	gtk_widget_show(button);
	
	/* circle 1 */
	button = gtk_button_new_with_label("circle 1");
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
			   (GtkSignalFunc)draw_circle1,NULL);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
			   button, TRUE, TRUE, 0);
	gtk_widget_show(button);

	/* circle 2 */
	button = gtk_button_new_with_label("circle 2");
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
			   (GtkSignalFunc)draw_circle2,NULL);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
			   button, TRUE, TRUE, 0);
	gtk_widget_show(button);

	
	/* display the dialog */
	gtk_widget_show(dialog);
}	


/*
 * main function
 */
int
main(int argc, char **argv)
{
	/* GtkWidget is the storage type for widgets */
	GtkWidget *dialog;
	GtkWidget *close_button;
	GtkWidget *square_button;
	GtkWidget *line_button;
	GtkWidget *circle_button;
	GtkWidget *window1;
	GtkWidget *window2;

	/*
	 * This is called in all GTK applications. Arguments are parsed from
	 * the command line and are returned to the application.
	 */
	gtk_init(&argc, &argv);

#if HAVE_DPS_NXAGENT    
  fprintf(stderr, "Set DPS NXAGENT to lauch automatically...");
  XDPSNXSetClientArg(XDPSNX_AUTO_LAUNCH, (void *)True);
  fprintf(stderr, "done\n");
#endif /* HAVE_DPS_NXAGENT */	

	/* create a new window */
	window1 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window1), "window 1");
	window2 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window2), "window 2");
	
	/* create a new dps area 1 */
	DPS_area1 = gtk_dps_area_new();
	gtk_container_add(GTK_CONTAINER(window1), DPS_area1);
	gtk_dps_area_size(GTK_DPS_AREA(DPS_area1), 300, 300);
	gtk_widget_set_events(DPS_area1, GDK_EXPOSURE_MASK);
	gtk_widget_show(DPS_area1);

	/* create a new dps area 2 */
	DPS_area2 = gtk_dps_area_new();
	gtk_container_add(GTK_CONTAINER(window2), DPS_area2);
	gtk_dps_area_size(GTK_DPS_AREA(DPS_area2), 300, 300);
	gtk_widget_set_events(DPS_area2, GDK_EXPOSURE_MASK);
	gtk_widget_show(DPS_area2);
	
	/* display this window */
	gtk_widget_show(window1);
	gtk_widget_show(window2);
	
	init_dialog();

	gtk_main();
	
	return 0;
}









