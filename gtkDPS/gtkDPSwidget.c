/* gtkDPSwidget.c
 * Copyright (C) 1997, 1998 GYVE Development Team
 *
 * Author: Terumoto 'tel' HAYAKAWA <hayakawa@cv.cs.ritsumei.ac.jp>
 * Created: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gtkDPScontext.h"
#include "gtkDPSwidget.h"
#include <gtk/gtksignal.h>

static void gtk_dps_widget_class_init    (GtkDPSWidgetClass *klass);
static void gtk_dps_widget_init          (GtkDPSWidget      *dps_widget);

guint
gtk_dps_widget_get_type ()
{
  static guint dps_widget_type = 0;

  if (!dps_widget_type)
    {
      GtkTypeInfo dps_widget_info =
	{
	  "GtkDPSWidget",
	  sizeof (GtkDPSWidget),
	  sizeof (GtkDPSWidgetClass),
	  (GtkClassInitFunc) gtk_dps_widget_class_init,
	  (GtkObjectInitFunc) gtk_dps_widget_init,
	  (GtkArgSetFunc) NULL,
	  (GtkArgGetFunc) NULL
	};

      dps_widget_type = gtk_type_unique (gtk_widget_get_type (),
					 &dps_widget_info);
    }

  return dps_widget_type;
}

static void
gtk_dps_widget_class_init (GtkDPSWidgetClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;
}

static void
gtk_dps_widget_init (GtkDPSWidget *dps_widget)
{
  GTK_WIDGET_SET_FLAGS (dps_widget, GTK_BASIC);
}


GtkDPSContext *
gtk_dps_widget_get_context (GtkDPSWidget *dps_widget)
{
  g_return_val_if_fail (dps_widget != NULL, (GtkDPSContext *)NULL);
  g_return_val_if_fail (GTK_IS_DPS_WIDGET (dps_widget), (GtkDPSContext *)NULL);

  return GTK_DPS_WIDGET (dps_widget)->gtk_dps_context;
}

