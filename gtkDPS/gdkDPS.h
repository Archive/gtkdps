/* gdkDPS.h
 * Copyright (C) 1997, 1998 GYVE Development Team
 *
 * Author:  Terumoto 'tel' HAYAKAWA <hayakawa@cv.cs.ritsumei.ac.jp>
 * Created: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef GDK__DPS_H
#define GDK__DPS_H


#include <gdk/gdk.h>
#include <DPS/dpsclient.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef struct _GdkDPSContext       GdkDPSContext;

struct _GdkDPSContext
{
  DPSContext *dps_context;
};


GdkDPSContext* gdk_dps_context_new        (GdkWindow *);
void           gdk_dps_context_destroy    (GdkDPSContext *);

void           gdk_dps_set_context(GdkDPSContext *);

gint           gdk_dps_set_current             (GdkDPSContext *, GdkDrawable *);
GdkDPSContext* gdk_dps_get_current_context     (void);
GdkDrawable*   gdk_dps_get_current_drawable    (void);

void           gdk_dps_flush_context(GdkDPSContext *);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* GDK__DPS_CONTEXT_H */
