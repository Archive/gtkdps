/* gtkDPS.h
 * Copyright (C) 1997, 1998 GYVE Development Team
 *
 * Author:  Terumoto 'tel' HAYAKAWA <hayakawa@cv.cs.ritsumei.ac.jp> 
 * Created: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GTK__DPS_H
#define GTK__DPS_H 


#include <DPS/dpsclient.h>


#include <gtkDPS/gtkDPScontext.h>

#include <gtkDPS/gtkDPSwidget.h>
#include <gtkDPS/gtkDPSarea.h>

const char * gtkDPS_version();

#endif /* GTK__DPS_H */
