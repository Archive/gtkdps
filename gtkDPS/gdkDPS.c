/* gdkDPS.c
 * Copyright (C) 1997, 1998 GYVE Development Team
 *
 * Author: Terumoto 'tel' HAYAKAWA <hayakawa@cv.cs.ritsumei.ac.jp>
 * Created: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gdk/gdkx.h>

#include <DPS/dpsclient.h>
#include <DPS/dpsXclient.h>
#include <DPS/dpsXshare.h>

#define GDK__DPS_CONTEXT_INTERNAL
#include "gdkDPS.h"


static GdkDPSContext *gdk_dps_current_gdk_dps_context = NULL;
static GdkDrawable   *gdk_dps_current_gdk_drawable    = NULL;


GdkDPSContext*
gdk_dps_context_new (GdkWindow *gdk_window) {
  GdkDPSContext *gdk_dps_context;
  GdkGC         *gc;
  Display       *x_display;
  Window         x_window;
  gint           width, height;

  g_return_val_if_fail(gdk_window != NULL, NULL);

  gdk_dps_context = g_new(GdkDPSContext, 1);

  gdk_dps_context->dps_context = g_new(DPSContext, 1);

  x_display = GDK_WINDOW_XDISPLAY(gdk_window),
  x_window  = GDK_WINDOW_XWINDOW(gdk_window),
  gc = gdk_gc_new(gdk_window);

  gdk_window_get_size(gdk_window, &width, &height);

  *(gdk_dps_context->dps_context)
       = XDPSCreateSimpleContext(x_display,
				 x_window,
				 GDK_GC_XGC(gc),
				 0, height, 
				 DPSDefaultTextBackstop,
				 DPSDefaultErrorProc, 
				 NULL);
  return gdk_dps_context;
}

void
gdk_dps_context_destroy(GdkDPSContext *gdk_dps_context)
{
  g_return_if_fail (gdk_dps_context != NULL);

  DPSDestroySpace( DPSSpaceFromContext(*(gdk_dps_context->dps_context)));

  g_free(gdk_dps_context->dps_context);
  g_free(gdk_dps_context);
}


void
gdk_dps_set_context(GdkDPSContext *gdk_dps_context)
{
  gdk_dps_current_gdk_dps_context = gdk_dps_context;

  DPSSetContext(*(gdk_dps_context->dps_context));
}


gint
gdk_dps_set_current(GdkDPSContext *gdk_dps_context, GdkDrawable *gdk_drawable)
{
  Display *x_display;
  Window   x_drawable;

  gint           width, height;
  Bool r;
  DPSContext dps_context;

  g_return_val_if_fail (gdk_dps_context != NULL, FALSE);
  g_return_val_if_fail (gdk_drawable != NULL, FALSE);

  gdk_window_get_size(gdk_drawable, &width, &height);

  x_display  = GDK_WINDOW_XDISPLAY(gdk_drawable);
  x_drawable = GDK_WINDOW_XWINDOW(gdk_drawable);

  dps_context = *(gdk_dps_context->dps_context);
  
  r = XDPSSetContextDrawable(dps_context, x_drawable, height);

  if (r == dps_status_success) {
      gdk_dps_current_gdk_dps_context = gdk_dps_context;
      gdk_dps_current_gdk_drawable = gdk_drawable;

      return TRUE;
  } else {
      return FALSE;
  }
}


GdkDPSContext *
gdk_dps_get_current_context(void)
{
  DPSContext tmp_dps_context;

  g_return_val_if_fail(gdk_dps_current_gdk_dps_context != NULL, NULL);


  tmp_dps_context = DPSGetCurrentContext();

  if (*(gdk_dps_current_gdk_dps_context->dps_context) != tmp_dps_context) {
    return NULL;
  } else {
    return gdk_dps_current_gdk_dps_context;
  }
}


GdkDrawable *
gdk_dps_get_current_drawable(void)
{
  return gdk_dps_current_gdk_drawable;
}


void
gdk_dps_flush_context(GdkDPSContext *gdk_dps_context)
{
  DPSFlushContext(*(gdk_dps_context->dps_context));
}
