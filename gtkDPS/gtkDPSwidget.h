/* gtkDPSwidget.h
 * Copyright (C) 1997, 1998 GYVE Development Team
 *
 * Author:  Terumoto 'tel' HAYAKAWA <hayakawa@cv.cs.ritsumei.ac.jp>
 * Created: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GTK__DPS_WIDGET_H
#define GTK__DPS_WIDGET_H 


#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>

#include <gtkDPS/gtkDPScontext.h>

/*#include <DPS/dpsXclient.h>*/

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_DPS_WIDGET(obj)          GTK_CHECK_CAST (obj, gtk_dps_widget_get_type (), GtkDPSWidget)
#define GTK_DPS_WIDGET_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_dps_widget_get_type (), GtkDPSWidgetClass)
#define GTK_IS_DPS_WIDGET(obj)       GTK_CHECK_TYPE (obj, gtk_dps_widget_get_type ())


typedef struct _GtkDPSWidget       GtkDPSWidget;
typedef struct _GtkDPSWidgetClass  GtkDPSWidgetClass;

struct _GtkDPSWidget
{
  GtkWidget     widget;

  GtkDPSContext *gtk_dps_context;
};

struct _GtkDPSWidgetClass
{
  GtkWidgetClass parent_class;

  void (* begin) (GtkDPSWidget *);
  void (* end)   (GtkDPSWidget *);
  void (* flush) (GtkDPSWidget *);
};


guint          gtk_dps_widget_get_type       (void);

GtkDPSContext* gtk_dps_widget_get_context    (GtkDPSWidget *);

void           gtk_dps_wrap_begin_with_widget    (GtkDPSWidget *);
void           gtk_dps_wrap_end_with_widget      (GtkDPSWidget *);

void           gtk_dps_flush_with_widget    (GtkDPSWidget *);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* GTK__DPS_WIDGET_H */
