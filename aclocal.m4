dnl aclocal.m4 generated automatically by aclocal 1.3

dnl Copyright (C) 1994, 1995, 1996, 1997, 1998 Free Software Foundation, Inc.
dnl This Makefile.in is free software; the Free Software Foundation
dnl gives unlimited permission to copy and/or distribute it,
dnl with or without modifications, as long as this notice is preserved.

dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY, to the extent permitted by law; without
dnl even the implied warranty of MERCHANTABILITY or FITNESS FOR A
dnl PARTICULAR PURPOSE.

# Do all the work for Automake.  This macro actually does too much --
# some checks are only needed if your package does certain things.
# But this isn't really a big deal.

# serial 1

dnl Usage:
dnl AM_INIT_AUTOMAKE(package,version, [no-define])

AC_DEFUN(AM_INIT_AUTOMAKE,
[AC_REQUIRE([AM_PROG_INSTALL])
PACKAGE=[$1]
AC_SUBST(PACKAGE)
VERSION=[$2]
AC_SUBST(VERSION)
dnl test to see if srcdir already configured
if test "`cd $srcdir && pwd`" != "`pwd`" && test -f $srcdir/config.status; then
  AC_MSG_ERROR([source directory already configured; run "make distclean" there first])
fi
ifelse([$3],,
AC_DEFINE_UNQUOTED(PACKAGE, "$PACKAGE")
AC_DEFINE_UNQUOTED(VERSION, "$VERSION"))
AC_REQUIRE([AM_SANITY_CHECK])
AC_REQUIRE([AC_ARG_PROGRAM])
dnl FIXME This is truly gross.
missing_dir=`cd $ac_aux_dir && pwd`
AM_MISSING_PROG(ACLOCAL, aclocal, $missing_dir)
AM_MISSING_PROG(AUTOCONF, autoconf, $missing_dir)
AM_MISSING_PROG(AUTOMAKE, automake, $missing_dir)
AM_MISSING_PROG(AUTOHEADER, autoheader, $missing_dir)
AM_MISSING_PROG(MAKEINFO, makeinfo, $missing_dir)
AC_REQUIRE([AC_PROG_MAKE_SET])])


# serial 1

AC_DEFUN(AM_PROG_INSTALL,
[AC_REQUIRE([AC_PROG_INSTALL])
test -z "$INSTALL_SCRIPT" && INSTALL_SCRIPT='${INSTALL_PROGRAM}'
AC_SUBST(INSTALL_SCRIPT)dnl
])

#
# Check to make sure that the build environment is sane.
#

AC_DEFUN(AM_SANITY_CHECK,
[AC_MSG_CHECKING([whether build environment is sane])
# Just in case
sleep 1
echo timestamp > conftestfile
# Do `set' in a subshell so we don't clobber the current shell's
# arguments.  Must try -L first in case configure is actually a
# symlink; some systems play weird games with the mod time of symlinks
# (eg FreeBSD returns the mod time of the symlink's containing
# directory).
if (
   set X `ls -Lt $srcdir/configure conftestfile 2> /dev/null`
   if test "[$]*" = "X"; then
      # -L didn't work.
      set X `ls -t $srcdir/configure conftestfile`
   fi
   if test "[$]*" != "X $srcdir/configure conftestfile" \
      && test "[$]*" != "X conftestfile $srcdir/configure"; then

      # If neither matched, then we have a broken ls.  This can happen
      # if, for instance, CONFIG_SHELL is bash and it inherits a
      # broken ls alias from the environment.  This has actually
      # happened.  Such a system could not be considered "sane".
      AC_MSG_ERROR([ls -t appears to fail.  Make sure there is not a broken
alias in your environment])
   fi

   test "[$]2" = conftestfile
   )
then
   # Ok.
   :
else
   AC_MSG_ERROR([newly created file is older than distributed files!
Check your system clock])
fi
rm -f conftest*
AC_MSG_RESULT(yes)])

dnl AM_MISSING_PROG(NAME, PROGRAM, DIRECTORY)
dnl The program must properly implement --version.
AC_DEFUN(AM_MISSING_PROG,
[AC_MSG_CHECKING(for working $2)
# Run test in a subshell; some versions of sh will print an error if
# an executable is not found, even if stderr is redirected.
# Redirect stdin to placate older versions of autoconf.  Sigh.
if ($2 --version) < /dev/null > /dev/null 2>&1; then
   $1=$2
   AC_MSG_RESULT(found)
else
   $1="$3/missing $2"
   AC_MSG_RESULT(missing)
fi
AC_SUBST($1)])

# Configure paths for GTK+
# Owen Taylor     97-11-3

dnl AM_PATH_GTK([MINIMUM-VERSION, [ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND]]])
dnl Test for GTK, and define GTK_CFLAGS and GTK_LIBS
dnl
AC_DEFUN(AM_PATH_GTK,
[dnl 
dnl Get the cflags and libraries from the gtk-config script
dnl
AC_ARG_WITH(gtk-prefix,[  --with-gtk-prefix=PFX   Prefix where GTK is installed (optional)],
            gtk_config_prefix="$withval", gtk_config_prefix="")
AC_ARG_WITH(gtk-exec-prefix,[  --with-gtk-exec-prefix=PFX Exec prefix where GTK is installed (optional)],
            gtk_config_exec_prefix="$withval", gtk_config_exec_prefix="")

  if test x$gtk_config_exec_prefix != x ; then
     gtk_config_args="$gtk_config_args --exec-prefix=$gtk_config_exec_prefix"
     if test x${GTK_CONFIG+set} != xset ; then
        GTK_CONFIG=$gtk_config_exec_prefix/bin/gtk-config
     fi
  fi
  if test x$gtk_config_prefix != x ; then
     gtk_config_args="$gtk_config_args --prefix=$gtk_config_prefix"
     if test x${GTK_CONFIG+set} != xset ; then
        GTK_CONFIG=$gtk_config_prefix/bin/gtk-config
     fi
  fi

  AC_PATH_PROG(GTK_CONFIG, gtk-config, no)
  min_gtk_version=ifelse([$1], ,0.99.7,$1)
  AC_MSG_CHECKING(for GTK - version >= $min_gtk_version)
  no_gtk=""
  if test "$GTK_CONFIG" != "no" ; then
    GTK_CFLAGS=`$GTK_CONFIG $gtk_config_args --cflags`
    GTK_LIBS=`$GTK_CONFIG $gtk_config_args --libs`
    ac_save_CFLAGS="$CFLAGS"
    ac_save_LIBS="$LIBS"
    CFLAGS="$CFLAGS $GTK_CFLAGS"
    LIBS="$LIBS $GTK_LIBS"
dnl
dnl Now check if the installed GTK is sufficiently new. (Also sanity
dnl checks the results of gtk-config to some extent
dnl
    AC_TRY_RUN([
#include <gtk/gtk.h>
#include <stdio.h>

int 
main ()
{
  int major, minor, micro;

  if (sscanf("$min_gtk_version", "%d.%d.%d", &major, &minor, &micro) != 3) {
     printf("%s, bad version string\n", "$min_gtk_version");
     exit(1);
   }

   return !((gtk_major_version > major) ||
   	    ((gtk_major_version == major) && (gtk_minor_version > minor)) ||
 	    ((gtk_major_version == major) && (gtk_minor_version == minor) && (gtk_micro_version >= micro)));
}
],, no_gtk=yes,[echo $ac_n "cross compiling; assumed OK... $ac_c"])
     CFLAGS="$ac_save_CFLAGS"
     LIBS="$ac_save_LIBS"
  else
     no_gtk=yes
  fi
  if test "x$no_gtk" = x ; then
     AC_MSG_RESULT(yes)
     ifelse([$2], , :, [$2])     
  else
     AC_MSG_RESULT(no)
     GTK_CFLAGS=""
     GTK_LIBS=""
     ifelse([$3], , :, [$3])
  fi
  AC_SUBST(GTK_CFLAGS)
  AC_SUBST(GTK_LIBS)
])

dnl path_dps.m4 --- test for dps client libraries/headers
dnl Copyright (C) 1998 GYVE Development Team
dnl 
dnl Author: Masatake YAMATO (masata-y@is.aist-nara.ac.jp)
dnl
dnl Tester: Hideki Fujimoto (hideki70@osk2.3web.ne.jp) on Linux.
dnl         Shin-ichi Okada (s-okada@ksd.co.jp) on DGS-Debian.
dnl         Yasuhiro Shirasaki (yasuhiro@awa.tohoku.ac.jp) on OSF1.
dnl         Masatake YAMATO (masata-y@is.aist-nara.ac.jp) on Solaris2, OSF1
dnl
dnl Created: Sat Jan 17 14:27:37 1998
dnl Time-stamp: <98/02/04 20:04:27 masata-y>
dnl
dnl This file is a spinoff of the GYVE vector based drawing editor.  
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2, or (at your option)
dnl any later version.
dnl
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software
dnl Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl
dnl As a special exception, the Free Software Foundation gives unlimited
dnl permission to copy, distribute and modify the configure scripts that
dnl are the output of Autoconf.  You need not follow the terms of the GNU
dnl General Public License when using or distributing such scripts, even
dnl though portions of the text of Autoconf appear in them.  The GNU
dnl General Public License (GPL) does govern all other use of the material
dnl that constitutes the Autoconf program.
dnl
dnl Certain portions of the Autoconf source text are designed to be copied
dnl (in certain cases, depending on the input) into the output of
dnl Autoconf.  We call these the "data" portions.  The rest of the Autoconf
dnl source text consists of comments plus executable code that decides which
dnl of the data portions to output in any given case.  We call these
dnl comments and executable code the "non-data" portions.  Autoconf never
dnl copies any of the non-data portions into its output.
dnl
dnl This special exception to the GPL applies to versions of Autoconf
dnl released by the Free Software Foundation.  When you make and
dnl distribute a modified version of Autoconf, you may extend this special
dnl exception to the GPL to apply to your modified version as well, *unless*
dnl your modified version has the potential to copy into its output some
dnl of the text that was the non-data portion of the version that you started
dnl with.  (In other words, unless your change moves or copies text from
dnl the non-data portions to the data portions.)  If your modification has
dnl such potential, you must delete any notice of this special exception
dnl to the GPL from your modified version.

dnl 
dnl ----------------------------------------------------------------
dnl * Description *
dnl ----------------------------------------------------------------
dnl This file contains three m4 macros, AC_PATH_DPS, AC_CHECK_DPS_NXAGENT
dnl and AC_PATH_DPSET.
dnl AC_PATH_DPS finds paths for the DPS client libraries and headers. 
dnl AC_CHECK_DPS_NXAGENT checks a dps nxagent is available or not.
dnl AC_APTH_DPSET invokes above two macros. Additionally this macro
dnl defines some variables.
dnl 
dnl
dnl 1. AC_PATH_DPS sets three shell variables, no_dps, dps_includes and 
dnl dps_libraries. 
dnl 
dnl * no_dps
dnl NULL string or "yes" is set. "yes" is set when AC_PATH_DPS cannot find
dnl the path for the DPS releated files or when "no" is specified as the
dnl argument for the option --with-dps of configure script. If AC_PATH_DPS
dnl can find the path, NULL string is set.
dnl
dnl * dps_includes
dnl The path for headers of dps client libraries is set to this variable.
dnl If --with-dps-includes=DIR are given as the flag for configure script,
dnl DIR is set to the variable.
dnl The value does not make sense when the value of no_dps is "yes".
dnl Pass the value to the C compiler through CPPFLAGS in your configure.in 
dnl and you can include DPS header files in your files.
dnl 
dnl NOTE: Strictly the DPS headers are in $dps_includes/DPS.
dnl       So you need to write cpp include directives like:
dnl       #include <DPS/dpsXclient.h>
dnl 
dnl       Don't write like:
dnl       #include <dpsXclient.h>
dnl       
dnl * dps_libraries 
dnl The path for dps client libraries is set to this variable.
dnl If --with-dps-libraries=DIR are given as the flag for configure script,
dnl DIR is set to the variable.
dnl The value does not make sense when the value of no_dps is "yes".
dnl Pass the value to the C compiler(or the linker) through LIBS in your
dnl configure.in and you can link the libdps.a, the DPS client libdps  with 
dnl your program.
dnl e.g.
dnl LIBS="-L$dps_libraries -ldps -$LIBS"
dnl NOTE: Don't forget to add the flags for X libraries to LIBS, too.
dnl 
dnl
dnl 2. AC_CHECK_DPS_NXAGENT checks the existence of the header file <DPS/dpsNXargs.h>
dnl and the function XDPSNXSetClientArg using AC_CHECK_HEADERS and AC_CHECK_FUNCS.
dnl So this macro defines HAVE_DPS_DPSNXARGS_H and HAVE_XDPSNXSETCLIENTARG that can
dnl be be referred from source files of your program if the header and function are
dnl existing.  
dnl For configure.in programmer, this macros defines three shell
dnl variables, no_dps_nxagent_header and no_dps_nxagent_func and no_dps_nxagent.
dnl
dnl * no_dps_nxagent_header
dnl NULL string or yes is set. yes is set if the header file is not existing.
dnl
dnl * no_dps_nxagent_func
dnl NULL string or yes is set. yes is set if the function is not existing.
dnl 
dnl * no_dps_nxagent
dnl NULL string or yes is set. yet is set if yet is set to no_dps_nxagent_header nor 
dnl no_dps_nxagent_func.
dnl
dnl
dnl 3. AC_PATH_DPSET is extended version of AC_PATH_DPS and AC_CHECK_DPS_NXAGENT.
dnl AC_PATH_DPSET sets no_dps, dps_includes, dps_libraries no_dps_nxagent_header,
dnl no_dps_nxagent_func and no_dps_nxagent which are explained 
dnl above. Additionally it sets DPS_CFLAGS, DPS_LIBS when no_dps is set to a NULL
dnl string like:
dnl DPS_CFLAGS=-I${dps_includes}
dnl DPS_LIBS="-L${dps_libraries} -ldpstk -ldps"
dnl It also defines HAVE_DPS_NXAGENT that will be passed to the C compiler if
dnl NULL string is set to no_dps_nxagent.
dnl
dnl Use AC_PATH_DPSET in your configure.in and C source files like this:
dnl --- configure.in ---
dnl AC_PATH_XTRA
dnl AC_PATH_DPSET
dnl if test "x$no_dps" = xyes ; then
dnl 	AC_MSG_ERROR(Could not find the dps client library: see config.log)
dnl fi
dnl CPPFLAGS="$CPPFLAGS $DPS_CFLAGS $X_CFLAGS"
dnl LIBS="$LIBS $DPS_LIBS $X_PRE_LIBS $X_LIBS -lXext -lX11 -lm $X_EXTRA_LIBS"
dnl --- C source file ---
dnl #ifdef HAVE_DPX_NXAGENT    
dnl #include <DPS/dpsNXargs.h>
dnl #endif
dnl ...
dnl #if HAVE_DPX_NXAGENT    
dnl   fprintf(stderr, "Does X server supports DPS extension...");
dnl   XDPSNXSetClientArg(XDPSNX_AUTO_LAUNCH, (void *)True);
dnl #endif
dnl ...
dnl --- end ---
dnl
dnl
dnl 4. How to mix this file with your package
dnl You need to include path_dps.m4 in your package.
dnl * If you use automake and  if you didn't install path_dps.m4
dnl   [1] Create a file named acinclude.m4 at the root directory of your package.
dnl   [2] Put follwoing codes in the acinclude.m4
dnl       include(path_dps.m4)
dnl   [3] Add path_dps.m4 to EXTRA_DIST in the Makefile.am at the directory .
dnl   [4] Invoke the follwoing commands at the directory:
dnl       automake; aclocal -I .; autoconf 
dnl
dnl * If you use automake and  if you installed path_dps.m4
dnl   [1] automake; autoconf 
dnl
dnl * If you use autoconf only:
dnl Use aclocal.m4. Read the info of autoconf. I tested path_dps.m4 only with
dnl automake.
dnl 
dnl 4. TODO
dnl English, checking libdpstk.a

dnl ----------------------------------------------------------------
dnl * private AC_PATH_DPS_GUESS_GNUSTEP(HEADER_or_LIBRARY)
dnl <Japanese>
dnl GNUSTEP MAKE $B%Q%C%1!<%8$r;H$C$F%$%s%9%H!<%k$7$?(Bdgs$B$N%Q%9$r?dDj$9$k(B. 
dnl $B0z?t$r0l$D<h$k(B. 
dnl  $1 == "HEADER"$B$J$i(B, dps_includes$B$r(B, 
dnl $B$=$l0J30$G$J$i(B, dps_libraries$B$r?dB,$9$k(B. 
dnl </Japanese>
dnl ----------------------------------------------------------------
AC_DEFUN(AC_PATH_DPS_GUESS_GNUSTEP, 
[dps_arch=${GNUSTEP_HOST_CPU}
dps_os=${GNUSTEP_HOST_OS}
dps_combo=${LIBRARY_COMBO}
#
if test x$1 = "xHEADER" ; then
	dps_includes=${GNUSTEP_SYSTEM_ROOT}/Headers/${dps_arch}/${dps_os}
else	
	if test x$dps_combo = "x"; then
		dps_libraries=${GNUSTEP_SYSTEM_ROOT}/Libraries/${dps_arch}/${dps_os}
	elif test -r "${GNUSTEP_SYSTEM_ROOT}/Libraries/${dps_arch}/${dps_os}/libdps.a"; then
		dps_libraries=${GNUSTEP_SYSTEM_ROOT}/Libraries/${dps_arch}/${dps_os}
	elif test -r "${GNUSTEP_SYSTEM_ROOT}/Libraries/${dps_arch}/${dps_os}/libdps.so"; then
		dps_libraries=${GNUSTEP_SYSTEM_ROOT}/Libraries/${dps_arch}/${dps_os}
	else
		dps_libraries=${GNUSTEP_SYSTEM_ROOT}/Libraries/${dps_arch}/${dps_os}/${dps_combo}
	fi
fi
no_dps=
])

dnl ----------------------------------------------------------------
dnl * private AC_PATH_DPS_GUESS(HEADER_or_LIBRARY)
dnl For vender's dps client library.
dnl <Japanese>no_dps, dps_includes, dps_libraries$B$K?dB,$7$?%Q%9$rBeF~$9$k(B.
dnl $B0z?t$r0l$D<h$k(B. 
dnl  $1 == "HEADER"$B$G$J$i(B, dps_includes$B$r(B, 
dnl $B$=$l0J30$G$J$i(B, dps_libraries$B$r?dB,$9$k(B. 
dnl </Japanese>
dnl ----------------------------------------------------------------
AC_DEFUN(AC_PATH_DPS_GUESS,
[case "$target" in 
dnl (
# Solaris2 has DPS.
# Including openwin headers directory if the OS is Solaris-2.x that has DPS.
# 
 	*-*-solaris2*)
                solaris_dps_root=${OPENWINHOME}
 		if test x$OPENWINHOME = "x"; then
			AC_MSG_ERROR(Set OPENWINHOME environment variable.)
                        no_dps=yes
		else
			if test "x$1" = xHEADER ; then
				dps_includes="$solaris_dps_root/include/X11"
			else
				dps_libraries="$solaris_dps_root/lib"
			fi
                        no_dps=
		fi
       ;;
dnl (
# OSF/DigitalUNIX? also has DPS.
# 
      	*-*-osf*)	
                osf_dps_root=/usr
		if test "x$1" = xHEADER ; then
			dps_includes="$osf_dps_root/include"
		elif test -r "$osf_dps_root/shlib/libdps.so" ; then
#
# Use shared library (if existing)
#
			dps_libraries="$osf_dps_root/shlib"
		else 
#
# Use staitc library 
#
			dps_libraries="$osf_dps_root/lib"
		fi
                no_dps=
        ;;
dnl (
# AIX also has DPS but we don't have AIX box so, we don't test this macro on
# that platform. If this works file, pls report the author
# (masata-y@is.aist-nara.ac.jp).
#
dnl The AIX related information provided by s-okada.
dnl <Japanese>
dnl > $B$=$l$+$i(B, AIX$B$N7o$O$I$&$G$7$g$&$+(B? >> $B2,ED$5$s(B. 
dnl 
dnl AIX3.X,4.X$B$H$"$C$?$N$G$9$,!$(B4.X$B$K$O%$%s%9%H!<%k$5$l$F$$$J$$$h$&$G$7$?!%(B
dnl $B$=$l$i$7$-%U%!%$%k$,$I$3$K$b$"$j$^$;$s$G$7$?!%%G%U%)%k%H$8$c$J$$$N$+$J!)(B
dnl $B0J2<$,(B3.X$B$N%$%s%9%H!<%k@h$G$9!%(B
dnl 
dnl AIX 3.2.5
dnl /usr/lpp/DPS/bin
dnl /usr/lpp/DPS/lib
dnl /usr/include/DPS
dnl 
dnl http://www.ibm.co.jp/manuals/gc23-3902-02/d3b63114-2.html
dnl 
dnl $B$K!V(BAIX 4.1 $B9=@.MWAG$N%j%9%H!W$H$$$&$b$N$,$"$j!$(BDPS$B$b:\$C$F$$$k$N$G!$(B
dnl 4.X$B$G$b(BDPS$B$,;H$($k$H;W$$$^$9!%(B
dnl </Japanese>
	*-*-aix*)
		if test "x$1" = xHEADER ; then
			dps_includes="/usr/include"
		else
			dps_libraries="/usr/lpp/DPS"
		fi
		no_dps=
	;;
dnl (
# Use /usr/local as default
#  
	*)
	if test x${GNUSTEP_SYSTEM_ROOT} = "x"; then
		if test "$1" = HEADER ; then
			dps_includes=/usr/local/include
		else
	 		dps_libraries=/usr/local/lib
		fi
 		no_dps=
	else
		if test "x$1" = xHEADER ; then
			AC_PATH_DPS_GUESS_GNUSTEP(HEADER)
		else
			AC_PATH_DPS_GUESS_GNUSTEP(LIBRARY)
		fi
	fi
	;;
esac])

dnl ----------------------------------------------------------------
dnl * private AC_PATH_DPS_CHECK_LIB
dnl Checking the existence of libdps.a
dnl If existing, set null string to no_dps.
dnl If not existing, set yes to no_dps.
dnl ----------------------------------------------------------------
AC_DEFUN(AC_PATH_DPS_CHECK_LIB,
[if test "x$no_dps" = xyes; then
dnl DO nothing
	echo -n "" 
elif test "x$no_x" = xyes; then
        no_dps=yes
elif test "x${x_libraries}" != x; then
   dps_check_lib_save_libs=$LIBS
   LIBS="-ldps -L${dps_libraries} -L${x_libraries} -lX11 -lm $LIBS"
   AC_TRY_LINK(, [XDPSCreateSimpleContext()], , no_dps=yes)
   LIBS=$dps_check_lib_save_libs
else
   dps_check_lib_save_libs=$LIBS
# We can compile using X headers with no special include directory.
   LIBS="-ldps -L${dps_libraries} -lX11 -lm $LIBS"
   AC_TRY_LINK(, [XDPSCreateSimpleContext()], , no_dps=yes)
   LIBS=$dps_check_lib_save_libs
fi])

dnl ----------------------------------------------------------------
dnl * private AC_PATH_DPS_CHECK_HEADER
dnl Checking the existence of DPS/*.h
dnl ----------------------------------------------------------------
AC_DEFUN(AC_PATH_DPS_CHECK_HEADER, 
[AC_REQUIRE_CPP()
if test "x$no_dps" = xyes ; then
dnl DO nothing
	echo -n "" 
else
	dps_check_lib_save_headers=$CPPFLAGS
	CPPFLAGS="-I${dps_includes} $CPPFLAGS"
	AC_TRY_CPP([#include <DPS/dpsfriends.h>], no_dps=, no_dps=yes)
	CPPFLAGS=$dps_check_lib_save_headers
fi])

dnl ----------------------------------------------------------------
dnl * private AC_PATH_DPS_CHECK
dnl ----------------------------------------------------------------
AC_DEFUN(AC_PATH_DPS_CHECK, 
[AC_PATH_DPS_CHECK_LIB 
if test "x$no_dps" != xyes ; then
	AC_PATH_DPS_CHECK_HEADER
fi])


dnl ----------------------------------------------------------------
dnl * public AC_PATH_DPS
dnl ----------------------------------------------------------------
AC_DEFUN(AC_PATH_DPS,
[dnl
AC_REQUIRE([AC_PATH_X])
AC_MSG_CHECKING(for DPS)
dnl ------------------------------- Initialize variables
dps_includes=NONE
dps_libraries=NONE

dnl ------------------------------- Option arguments
AC_ARG_WITH(dps,           
	[  --with-dps              use the DPS. Give yes or no. [yes]], ,
                                   with_dps=yes)

AC_ARG_WITH(dps_includes,  
	[  --with-dps-includes=DIR  DPS include files are in DIR/DPS], ,
	with_dps_includes=NONE)

AC_ARG_WITH(dps_libraries, 
	[  --with-dps-libraries=DIR DPS libraries files are in DIR], ,
	with_dps_libraries=NONE)	

dnl ------------------------------- Check the arguments for configure 
dnl ------------------------------- And decide dps_includes&dps_libraries.
if test "x$with_dps" = xno; then
  no_dps=yes
else
  if test "x$dps_includes" != xNONE && test "x$dps_libraries" != xNONE; then
    no_dps=
  else	
    AC_CACHE_VAL(ac_cv_path_dps,
    [dnl ---------------------------- Set defalut value
    no_dps=yes
  
    if test "x$with_dps_includes" = xNONE ; then
  	AC_PATH_DPS_GUESS(HEADER)
    else
  	no_dps=
  	dps_includes=$with_dps_includes
    fi
  
    if test "x$with_dps_libraries" = xNONE ; then
   	AC_PATH_DPS_GUESS(LIBRARY)
    else
	no_dps=
  	dps_libraries=$with_dps_libraries
    fi

dnl ------------------------------- Test
    AC_PATH_DPS_CHECK
dnl ------------------------------- Generate value for caching
    if test "x$no_dps" = xyes; then
      ac_cv_path_dps="no_dps=yes"
    else
      ac_cv_path_dps="no_dps= ac_dps_includes=$ac_dps_includes ac_dps_libraries=$ac_dps_libraries"
    fi])
dnl ------------------------------- Eval cache value
  fi
  eval "$ac_cv_path_dps"
dnl ------------------------------- Report the result
  if test "x$no_dps" = xyes; then
    AC_MSG_RESULT(no)
  else
    test "x$dps_includes" = xNONE && dps_includes=$ac_dps_includes
    test "x$dps_libraries" = xNONE && dps_libraries=$ac_dps_libraries
    ac_cv_path_dps="no_dps= ac_dps_includes=$dps_includes ac_dps_libraries=$dps_libraries"
    AC_MSG_RESULT([libraries $dps_libraries, headers $dps_includes])
  fi
fi])

dnl ----------------------------------------------------------------
dnl * public AC_CHECK_DPS_NXAGENT
dnl ----------------------------------------------------------------
AC_DEFUN(AC_CHECK_DPS_NXAGENT,
[AC_REQUIRE([AC_PATH_DPS])
if test "x$no_dps" = xyes ; then
dnl Do nothing
	echo -n ""	
else
 	dps_check_lib_save_headers=$CPPFLAGS	
	CPPFLAGS="-I${dps_includes} $CPPFLAGS"
	AC_CHECK_HEADERS(DPS/dpsNXargs.h,
		 no_dps_nxagent_header=, no_dps_nxagent_header=yes)
	CPPFLAGS=$dps_check_lib_save_headers
	
	dps_check_lib_save_libs=$LIBS	
        if test "x${x_libraries}" != x; then
	  LIBS="-ldpstk -ldps -L${dps_libraries} -L${x_libraries} -lX11 -lm $LIBS"
        else
          LIBS="-ldpstk -ldps -L${dps_libraries} -lX11 -lm $LIBS"
        fi
	AC_CHECK_FUNCS(XDPSNXSetClientArg,
		no_dps_nxagent_func=,no_dps_nxagent_func=yes)
	LIBS=$dps_check_lib_save_libs	
	AC_MSG_CHECKING(for DPS NXAGENT)
	if test "x$no_dps_nxagent_header" != xyes && test "x$no_dps_nxagent_func" != xyes ; then
		no_dps_nxagent=
		AC_MSG_RESULT(yes)
	else
		no_dps_nxagent=yes
		AC_MSG_RESULT(no)
	fi
fi])
dnl ----------------------------------------------------------------
dnl * public AC_PATH_DPSET
dnl ----------------------------------------------------------------
AC_DEFUN(AC_PATH_DPSET, 
[AC_REQUIRE([AC_PATH_DPS])
AC_REQUIRE([AC_CHECK_DPS_NXAGENT])
if test "x$no_dps" = xyes ; then
dnl Do nothing
	echo -n ""	
else
	if test "x$no_dps_nxagent" != xyes ; then
		AC_DEFINE(HAVE_DPS_NXAGENT)
	fi
	DPS_CFLAGS=-I${dps_includes}
        DPS_LIBS="-L${dps_libraries} -ldpstk -ldps"
fi
])

dnl ----------------------------------------------------------------
dnl About DPS server and DPS client library.
dnl ----------------------------------------------------------------
dnl
dnl [0] Solaris2, AIX and OSF1 have them. Lucky you!
dnl
dnl [1] HPUX and IRIX have the DPS extension on its X server 
dnl     but I cannot find the client side files for DPS.
dnl     I found libdps.so in /usr/lib on IRIX. But I cannot find header
dnl     files. 
dnl     You should have to install DPS client library by your self.
dnl     The client library is provided by ADOBE. I don't know well about
dnl     it.
dnl 
dnl     [/X11R5/contrib/lib/DPS/include/DPS]
dnl     [/X11R6/contrib/lib/DPS/include/DPS]
dnl
dnl [2] etc... You can check the existence of DPS library by follwing shell 
dnl            command line.
dnl     % which pswrap
dnl     If the command tells the path for pswrap command, the system you are
dnl     using has DPS client library.
dnl       
dnl [3] *BSD, Linux are waitting for the free DPS server implementation 
dnl     named DGS. DGS will be part of gs5.50. Wait!
dnl 
dnl [4] NeXT/Apple/Rapsody?
dnl     I don't know well about it.
dnl
dnl Snapshot of the DGS is already released. It is named dgs-5.20.
dnl Count 30!
dnl Here, The directory structcture of dgs-5.20 is reported by s-okada.
dnl I've used this report to write macros in this file.
dnl
dnl <end of path_dps.m4>


# serial 24 AM_PROG_LIBTOOL
AC_DEFUN(AM_PROG_LIBTOOL,
[AC_REQUIRE([AM_ENABLE_SHARED])dnl
AC_REQUIRE([AM_ENABLE_STATIC])dnl
AC_REQUIRE([AC_CANONICAL_HOST])dnl
AC_REQUIRE([AC_PROG_RANLIB])dnl
AC_REQUIRE([AC_PROG_CC])dnl
AC_REQUIRE([AM_PROG_LD])dnl
AC_REQUIRE([AM_PROG_NM])dnl
AC_REQUIRE([AC_PROG_LN_S])dnl
dnl
# Always use our own libtool.
LIBTOOL='$(SHELL) $(top_builddir)/libtool'
AC_SUBST(LIBTOOL)dnl

# Check for any special flags to pass to ltconfig.
libtool_flags=
test "$enable_shared" = no && libtool_flags="$libtool_flags --disable-shared"
test "$enable_static" = no && libtool_flags="$libtool_flags --disable-static"
test "$silent" = yes && libtool_flags="$libtool_flags --silent"
test "$ac_cv_prog_gcc" = yes && libtool_flags="$libtool_flags --with-gcc"
test "$ac_cv_prog_gnu_ld" = yes && libtool_flags="$libtool_flags --with-gnu-ld"

# Some flags need to be propagated to the compiler or linker for good
# libtool support.
case "$host" in
*-*-irix6*)
  # Find out which ABI we are using.
  echo '[#]line __oline__ "configure"' > conftest.$ac_ext
  if AC_TRY_EVAL(ac_compile); then
    case "`/usr/bin/file conftest.o`" in
    *32-bit*)
      LD="${LD-ld} -32"
      ;;
    *N32*)
      LD="${LD-ld} -n32"
      ;;
    *64-bit*)
      LD="${LD-ld} -64"
      ;;
    esac
  fi
  rm -rf conftest*
  ;;

*-*-sco3.2v5*)
  # On SCO OpenServer 5, we need -belf to get full-featured binaries.
  CFLAGS="$CFLAGS -belf"
  ;;
esac

# Actually configure libtool.  ac_aux_dir is where install-sh is found.
CC="$CC" CFLAGS="$CFLAGS" CPPFLAGS="$CPPFLAGS" \
LD="$LD" NM="$NM" RANLIB="$RANLIB" LN_S="$LN_S" \
${CONFIG_SHELL-/bin/sh} $ac_aux_dir/ltconfig \
$libtool_flags --no-verify $ac_aux_dir/ltmain.sh $host \
|| AC_MSG_ERROR([libtool configure failed])
])

# AM_ENABLE_SHARED - implement the --enable-shared flag
# Usage: AM_ENABLE_SHARED[(DEFAULT)]
#   Where DEFAULT is either `yes' or `no'.  If omitted, it defaults to
#   `yes'.
AC_DEFUN(AM_ENABLE_SHARED,
[define([AM_ENABLE_SHARED_DEFAULT], ifelse($1, no, no, yes))dnl
AC_ARG_ENABLE(shared,
changequote(<<, >>)dnl
<<  --enable-shared         build shared libraries [default=>>AM_ENABLE_SHARED_DEFAULT]
changequote([, ])dnl
[  --enable-shared=PKGS    only build shared libraries if the current package
                          appears as an element in the PKGS list],
[p=${PACKAGE-default}
case "$enableval" in
yes) enable_shared=yes ;;
no) enable_shared=no ;;
*)
  enable_shared=no
  # Look at the argument we got.  We use all the common list separators.
  IFS="${IFS= 	}"; ac_save_ifs="$IFS"; IFS="${IFS}:,"
  for pkg in $enableval; do
    if test "X$pkg" = "X$p"; then
      enable_shared=yes
    fi
  done
  IFS="$ac_save_ifs"
  ;;
esac],
enable_shared=AM_ENABLE_SHARED_DEFAULT)dnl
])

# AM_DISABLE_SHARED - set the default shared flag to --disable-shared
AC_DEFUN(AM_DISABLE_SHARED,
[AM_ENABLE_SHARED(no)])

# AM_DISABLE_STATIC - set the default static flag to --disable-static
AC_DEFUN(AM_DISABLE_STATIC,
[AM_ENABLE_STATIC(no)])

# AM_ENABLE_STATIC - implement the --enable-static flag
# Usage: AM_ENABLE_STATIC[(DEFAULT)]
#   Where DEFAULT is either `yes' or `no'.  If omitted, it defaults to
#   `yes'.
AC_DEFUN(AM_ENABLE_STATIC,
[define([AM_ENABLE_STATIC_DEFAULT], ifelse($1, no, no, yes))dnl
AC_ARG_ENABLE(static,
changequote(<<, >>)dnl
<<  --enable-static         build static libraries [default=>>AM_ENABLE_STATIC_DEFAULT]
changequote([, ])dnl
[  --enable-static=PKGS    only build shared libraries if the current package
                          appears as an element in the PKGS list],
[p=${PACKAGE-default}
case "$enableval" in
yes) enable_static=yes ;;
no) enable_static=no ;;
*)
  enable_static=no
  # Look at the argument we got.  We use all the common list separators.
  IFS="${IFS= 	}"; ac_save_ifs="$IFS"; IFS="${IFS}:,"
  for pkg in $enableval; do
    if test "X$pkg" = "X$p"; then
      enable_static=yes
    fi
  done
  IFS="$ac_save_ifs"
  ;;
esac],
enable_static=AM_ENABLE_STATIC_DEFAULT)dnl
])


# AM_PROG_LD - find the path to the GNU or non-GNU linker
AC_DEFUN(AM_PROG_LD,
[AC_ARG_WITH(gnu-ld,
[  --with-gnu-ld           assume the C compiler uses GNU ld [default=no]],
test "$withval" = no || with_gnu_ld=yes, with_gnu_ld=no)
AC_REQUIRE([AC_PROG_CC])
ac_prog=ld
if test "$ac_cv_prog_gcc" = yes; then
  # Check if gcc -print-prog-name=ld gives a path.
  AC_MSG_CHECKING([for ld used by GCC])
  ac_prog=`($CC -print-prog-name=ld) 2>&5`
  case "$ac_prog" in
  # Accept absolute paths.
  /* | [A-Za-z]:\\*)
    test -z "$LD" && LD="$ac_prog"
    ;;
  "")
    # If it fails, then pretend we aren't using GCC.
    ac_prog=ld
    ;;
  *)
    # If it is relative, then search for the first ld in PATH.
    with_gnu_ld=unknown
    ;;
  esac
elif test "$with_gnu_ld" = yes; then
  AC_MSG_CHECKING([for GNU ld])
else
  AC_MSG_CHECKING([for non-GNU ld])
fi
AC_CACHE_VAL(ac_cv_path_LD,
[if test -z "$LD"; then
  IFS="${IFS= 	}"; ac_save_ifs="$IFS"; IFS="${IFS}:"
  for ac_dir in $PATH; do
    test -z "$ac_dir" && ac_dir=.
    if test -f "$ac_dir/$ac_prog"; then
      ac_cv_path_LD="$ac_dir/$ac_prog"
      # Check to see if the program is GNU ld.  I'd rather use --version,
      # but apparently some GNU ld's only accept -v.
      # Break only if it was the GNU/non-GNU ld that we prefer.
      if "$ac_cv_path_LD" -v 2>&1 < /dev/null | egrep '(GNU|with BFD)' > /dev/null; then
	test "$with_gnu_ld" != no && break
      else
        test "$with_gnu_ld" != yes && break
      fi
    fi
  done
  IFS="$ac_save_ifs"
else
  ac_cv_path_LD="$LD" # Let the user override the test with a path.
fi])
LD="$ac_cv_path_LD"
if test -n "$LD"; then
  AC_MSG_RESULT($LD)
else
  AC_MSG_RESULT(no)
fi
test -z "$LD" && AC_MSG_ERROR([no acceptable ld found in \$PATH])
AC_SUBST(LD)
AM_PROG_LD_GNU
])

AC_DEFUN(AM_PROG_LD_GNU,
[AC_CACHE_CHECK([if the linker ($LD) is GNU ld], ac_cv_prog_gnu_ld,
[# I'd rather use --version here, but apparently some GNU ld's only accept -v.
if $LD -v 2>&1 </dev/null | egrep '(GNU|with BFD)' 1>&5; then
  ac_cv_prog_gnu_ld=yes
else
  ac_cv_prog_gnu_ld=no
fi])
])

# AM_PROG_NM - find the path to a BSD-compatible name lister
AC_DEFUN(AM_PROG_NM,
[AC_MSG_CHECKING([for BSD-compatible nm])
AC_CACHE_VAL(ac_cv_path_NM,
[case "$NM" in
/* | [A-Za-z]:\\*)
  ac_cv_path_NM="$NM" # Let the user override the test with a path.
  ;;
*)
  IFS="${IFS= 	}"; ac_save_ifs="$IFS"; IFS="${IFS}:"
  for ac_dir in /usr/ucb /usr/ccs/bin $PATH /bin; do
    test -z "$ac_dir" && ac_dir=.
    if test -f $ac_dir/nm; then
      # Check to see if the nm accepts a BSD-compat flag.
      # Adding the `sed 1q' prevents false positives on HP-UX, which says:
      #   nm: unknown option "B" ignored
      if ($ac_dir/nm -B /dev/null 2>&1 | sed '1q'; exit 0) | egrep /dev/null >/dev/null; then
        ac_cv_path_NM="$ac_dir/nm -B"
      elif ($ac_dir/nm -p /dev/null 2>&1 | sed '1q'; exit 0) | egrep /dev/null >/dev/null; then
        ac_cv_path_NM="$ac_dir/nm -p"
      else
        ac_cv_path_NM="$ac_dir/nm"
      fi
      break
    fi
  done
  IFS="$ac_save_ifs"
  test -z "$ac_cv_path_NM" && ac_cv_path_NM=nm
  ;;
esac])
NM="$ac_cv_path_NM"
AC_MSG_RESULT([$NM])
AC_SUBST(NM)
])

